<!-- #footer -->
    <div id="footer">
    </div>
    <!-- #footer -->


    <!-- Load Javascript Libraries -->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/plugins/CSSPlugin.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/plugins/ScrollToPlugin.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>


    <!-- Load JSON Config Data -->
    <script type="text/javascript" src="assets/data/data-config.js"></script> <!-- temp data -->


    <!-- Load Javascript Modules -->
    <script type="text/javascript" src="assets/js/main.js"></script>
    <script type="text/javascript" src="assets/js/modules/module-layout.js"></script>
    <script type="text/javascript" src="assets/js/modules/module-main-ui.js"></script>
    <script type="text/javascript" src="assets/js/modules/module-template-loader.js"></script>
    <script type="text/javascript" src="assets/js/modules/module-visual-code-history.js"></script>
	<script type="text/javascript" src="assets/js/modules/module-blocks.js"></script>
	<script type="text/javascript" src="assets/js/modules/module-video-hover-block.js"></script>
    


    <!-- Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-32014035-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-32014035-1');
    </script>
