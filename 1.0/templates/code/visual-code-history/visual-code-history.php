<?php
/*
Template Name: Visual Code History
*/
?>

<style>
<?php include 'visual-code-history.css'; ?>
</style>

<!-- #visual-code-history -->
<div class="title visual-code-history-title">Tools o' the trade.</div>
<div id="visual-code-history" class="block row">
	
	<div class="code-category col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">
		<div class="code-item">
			<div class="title">Javascript</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">Python</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">PHP</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">C#</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">Ruby</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">AS3</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">HTML/CSS</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
	</div>


	<div class="code-category col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">
		<div class="code-item">
			<div class="title">Unity</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">Tornado</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">Django</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">Wordpress</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">Bootstrap</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">JQuery</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">Rails</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">Angular</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="code-category col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">
		<div class="code-item">
			<div class="title">MySQL</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>

		<div class="code-item">
			<div class="title">MongoDB</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">PostgreSQL</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">AWS/EC2/RDS/S3</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">LAMP</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">Subversion</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
		<div class="code-item">
			<div class="title">Git</div>
			<div class="history"></div>
			<div class="exp-bar">
				<div class="exp-bar-wrapper">
					<div class="exp-bar-rating"></div>
					<div class="exp-bar-full"></div>
				</div>
			</div>
		</div>
	</div>



	

	

</div>
<!-- #visual-code-history -->