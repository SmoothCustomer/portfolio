<?php
/*
Template Name: Visual Code History
*/
?>

<style>
<?php include 'visual-code-history.css'; ?>
</style>

<!-- #visual-code-history -->
<div class="title visual-code-history-title">Log 01. Mike AI.</div>
<div class="title visual-code-history-title">Log 02. Setting up some pieces.</div>
<div class="title visual-code-history-title">Log 03. First steps.</div>
<div class="title visual-code-history-title">Log 04. I remember you.</div>

<div class="row">
	<div class="code-category col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
		<p>Fusce fringilla rutrum magna at suscipit. Cras dapibus augue quis erat semper ornare. Integer blandit sapien eros, vitae tristique eros fermentum ut. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras mi nibh, pretium eget maximus sed, feugiat vitae massa. Integer egestas vitae lorem eget mattis. Sed aliquam eleifend metus, id auctor nisl maximus nec. Fusce risus orci, cursus quis ligula in, varius dignissim quam. Praesent sed semper nisi. Donec ac elit leo. Aliquam congue fermentum sollicitudin. Nulla nisl tellus, ultricies non orci vitae, efficitur porttitor dolor. Donec enim eros, aliquet vitae efficitur at, interdum ac est.</p>
	</div>
	<div class="code-category col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris malesuada lorem vel sapien consequat, lobortis consectetur purus egestas. Nulla facilisi. Nunc convallis elit tempus dictum pretium. Nullam finibus justo eu nulla ultrices, ut aliquam est rhoncus. Vestibulum elementum, justo vitae suscipit interdum, lectus diam pellentesque erat, ut sodales lacus dolor id sapien. Vivamus ac eros ut lectus dignissim varius ac et mi. Vestibulum sit amet erat vulputate est dapibus posuere pharetra sed ligula. Aliquam mi elit, sagittis at urna non, ullamcorper tincidunt dolor. Donec finibus risus ac nisl ultrices vestibulum. Praesent rhoncus vitae nibh gravida venenatis.</p>
	</div>
</div>
<!-- #visual-code-history -->