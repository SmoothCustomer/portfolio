<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'home.css'; ?>
</style>


<!-- .template.home -->
<div class="template home">


	<div class="promo-blocks row base">

		<!-- .block -->
		<div class="promo-block left block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">

			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">ux.</div>
				</div>
			</div>

		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block right block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">

			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-content">
						<div class="promo-block-title">design.</div>
					</div>
				</div>
			</div>

		</div>
		<!-- .block -->
	</div>


	<div class="promo-blocks row base">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<div class="promo-block-inner-wrapper half-square-horizontal">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">code.</div>
				</div>
			</div>

		</div>
		<!-- .block -->

	</div>



	<div class="row base">

		<!-- .block -->
		<div class="new block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title">new things.</div>
		</div>
		<!-- .block -->

	</div>


	<div class="row base">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">hi.</div>
				</div>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">hi.</div>
				</div>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">hi.</div>
				</div>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">hi.</div>
				</div>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">hi.</div>
				</div>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">hi.</div>
				</div>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">hi.</div>
				</div>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">hi.</div>
				</div>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square">
				<div class="promo-block-content full-frame-image" style="background-image: url();">
					<div class="promo-block-title">hi.</div>
				</div>
			</div>
		</div>
		<!-- .block -->

	</div>


</div>
<!-- .template.home -->