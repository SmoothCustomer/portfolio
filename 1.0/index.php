<?php
    // Mobile Detection
    require_once 'assets/php_libs/Mobile-Detect-2.8.26/Mobile_Detect.php';
    $detect = new Mobile_Detect;
?>
<?php include 'templates/global/header.php'; ?>

<body>
    
    <!-- #viewport -->
    <div id="viewport">

    	<!-- #masthead -->
    	<div id="masthead">
    		<div id="main-ui">
    			
    			<div id="logo"><a class="route" href="/home" alt="home"><img src="assets/images/logo.svg" alt="logo"></a></div>

    			<div id="main-menu">
    				<div class="menu-button"><a class="route" href="/ux" alt="ux">ux.</a></div>
                    <div class="menu-button"><a class="route" href="/design" alt="design">design.</a></div>
                    <div class="menu-button"><a class="route" href="/code" alt="code">code.</a></div>
                    <div class="menu-button"><a class="route" href="/me" alt="me">me.</a></div>
    			</div>


                <div id="secondary-menu">
                    <!--
                    <div id="back">
                        <div class="menu-button"><a class="route" href="/home" alt="home">Back to home.</a></div>
                    </div>
                    -->
        			<div id="email">
        				<div class="menu-button"><a href="/email" alt="email">email.</a></div>
        			</div>
                </div>

    		</div>
    	</div>

    	<!-- #template -->
    	<div id="template" class="empty">
    	</div>

    </div>
    <!-- #viewport -->

    <?php include 'templates/global/footer.php'; ?>
    
</body>
</html>
