/*
 * layout module
 */
var layout = (function () {
 
 	// Private
 	// ---------------------------------------------------------

	var viewportWidth, viewportHeight;
	var layoutType, layoutOrientation;
	var orientationTimer, resizeTimer;
	var resizeDelay = 0;
	var orientationDelay = 200;
	var loadedModules;
	var breakPoint1 = 768;
	var breakPoint2 = 992;
	var breakPoint3 = 1230;


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('layout.initModule()');

		// Prevent bounce effect on iOS
    	document.ontouchmove = function(event){
	    	event.preventDefault();
		}
		initResize();
	};


	/*
	 * updateDimensions
	 * Set browser dimensions
	 */
	var updateDimensions = function() {
		document.body.style.overflow = "hidden";
		viewportWidth = $(window).width();
		viewportHeight = $(window).height();
		document.body.style.overflow = "";
	};


	/*
	 * updateLayoutType
	 * Sets layout size tag
	 */
	var updateLayoutType = function() {
		//window.console && console.log('layout.updateLayoutType()');

    	if( viewportWidth < breakPoint1 ){
    		layoutType = 'xs';
    	} else if( viewportWidth >= breakPoint1 && viewportWidth < breakPoint2 ){
    		layoutType = 'sm';
    	} else if( viewportWidth >= breakPoint2 && viewportWidth < breakPoint3 ){
    		layoutType = 'md';
    	} else {
    		layoutType = 'lg';
    	}
	};


	/*
	 * updateLayoutOrientation
	 * Sets layout orientation tag
	 */
	var updateLayoutOrientation = function() {
		//window.console && console.log('layout.updateLayoutOrientation()');

    	if( viewportHeight < viewportWidth ){
    		if( layoutOrientation != 'landscape' ){
    			layoutOrientation = 'landscape';
    			$(document).trigger( "layoutOrientationChange" );
    		}
    	} else {
    		if( layoutOrientation != 'portrait' ){
    			layoutOrientation = 'portrait';
    			$(document).trigger( "layoutOrientationChange" );
    		}
    	}
	};


	/*
	 * initResize
	 * Create viewport resizing events
	 */
	var initResize = function(){
		//window.console && console.log('layout.initResize()');

		// Initial resize event on page load
		resizeHandler();

		// Window resize event
		$(window).resize( function() {
			clearTimeout(resizeTimer);
	  		resizeTimer = setTimeout(resizeHandler, resizeDelay);
		});

		// Mobile orientation change event
		var supportsOrientationChange = "onorientationchange" in window,
		    orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

		if ( window.addEventListener ) {
			window.addEventListener(orientationEvent, function() {
				var orientation = window.orientation;
				clearTimeout(orientationTimer);
		        orientationTimer = setTimeout(function () {
					clearTimeout(resizeTimer);
		  			resizeTimer = setTimeout(resizeHandler, resizeDelay);
		    	}, orientationDelay);
		        
			}, false);
		}
	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function(){
		//window.console && console.log('layout.resizeHandler() *****************');
		
		// Get list of loaded js modules for resize event
		loadedModules = app.getLoadedModules();

		updateDimensions();
		updateLayoutType();
		updateLayoutOrientation();
		setLayoutClasses();
		resizeElements();
	};


	/*
	 * setLayoutClasses
	 * Appends layout and orientation tags as classes to <body>
	 */
	var setLayoutClasses = function(){
		//window.console && console.log('layout.setLayoutClasses()');

		// Remove all classes from body
		$('body').removeClass('lg md sm xs landscape portrait');

		// Set layout and orientation classes
		$('body').addClass(layoutType);
		$('body').addClass(layoutOrientation);
	};


	/*
	 * resizeElements
	 * on page resize checks to see which modules are
	 * loaded so only elements of current page are resized
	 */
	var resizeElements = function(){
		//window.console && console.log(' ');
		//window.console && console.log('loadedModules: '+JSON.stringify(loadedModules));

		// Loop through the loaded modules and call each module resize method
		for( var i = 0; i < loadedModules.length; i++ ) {
			if(loadedModules[i].name != 'layout'){
				loadedModules[i].resize();
			}
		}
	};


 	// Public
 	// ---------------------------------------------------------
	return {
		// Module name id for dynamic initialization
		name: 'layout',

		/*
		 * init
		 * Main init function called when
		 * module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('layout.init()');

			initModule();
	    },

	    /*
		 * getLayoutType
		 */
	    getLayoutType: function(){
	    	//window.console && console.log('layout.getLayoutType()');
	    	return layoutType;
	    },

	    /*
		 * getLayoutOrientation
		 */
	    getLayoutOrientation: function(){
	    	//window.console && console.log('layout.getLayoutOrientation()');
	    	return layoutOrientation;
	    },

	    /*
		 * getViewportWidth
		 */
	    getViewportWidth: function(){
	    	return viewportWidth;
	    },

	    /*
		 * getViewportHeight
		 */
	    getViewportHeight: function(){
	    	return viewportHeight;
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('layout.resize()');
	    	resizeHandler();
	    }
	};
})();
