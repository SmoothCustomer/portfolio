/*
 * blocks module
 */
var blocks = (function () {
 
 	// Private
 	// ---------------------------------------------------------
	var promoBlockPadding = 20;


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('blocks.initModule()');

	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('blocks.resizeHandler()');


		// Promo block content
		$('.promo-block-content').each(function () {
			var containerWidth = $(this).parent().outerWidth();
			var containerHeight = $(this).parent().outerHeight();
			var contentWidth = containerWidth - promoBlockPadding;
			var contentHeight = containerHeight - promoBlockPadding;

			$(this).css({
				'width': contentWidth,
				'height': contentHeight,
				'left': promoBlockPadding / 2,
				'top': promoBlockPadding / 2
			});
		});

	};


 	// Public
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'blocks',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('blocks.init()');

	    	initModule();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('blocks.resize()');
	    	resizeHandler();
	    }
	};
})();
