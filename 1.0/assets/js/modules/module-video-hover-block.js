/*
 * videoHoverBlock module
 */
var videoHoverBlock = (function () {
 
 	// Private
 	// ---------------------------------------------------------


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('videoHoverBlock.initModule()');

	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('videoHoverBlock.resizeHandler()');


	};


 	// Public
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'videoHoverBlock',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('videoHoverBlock.init()');

	    	initModule();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('visualCodeHistory.resize()');
	    	resizeHandler();
	    }
	};
})();
