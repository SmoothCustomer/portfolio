/*
 * visualCodeHistory module
 */
var visualCodeHistory = (function () {
 
 	// Private
 	// ---------------------------------------------------------


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('visualCodeHistory.initModule()');

	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('visualCodeHistory.resizeHandler()');


	};


 	// Public
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'visualCodeHistory',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('visualCodeHistory.init()');

	    	initModule();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('visualCodeHistory.resize()');
	    	resizeHandler();
	    }
	};
})();
