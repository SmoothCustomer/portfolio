/*
 * mainUI module
 */
var mainUI = (function () {
 
 	// Private
 	// ---------------------------------------------------------


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('mainUI.initModule()');

		initMainNav();
	};


	/*
	 * initMainNav
	 */
	var initMainNav = function() {
		window.console && console.log('mainUI.initMainNav()');


    	$('#main-ui a').on('click', function(event){
    		window.console && console.log('.menu-button click');

    		if( $(this).hasClass('route') ){
    			event.preventDefault();
    			var route = $(this).attr('href');
    			templateLoader.load(route);
    		}
    		
    	})

	};



	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('mainUI.resizeHandler()');


	};


 	// Public
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'mainUI',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('mainUI.init()');

	    	initModule();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('mainUI.resize()');
	    	resizeHandler();
	    }
	};
})();
