/*
 * templateLoader module
 */
var templateLoader = (function () {
 
 	// Private
 	// ---------------------------------------------------------

 	var templateDefaultID;

	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('templateLoader.initModule()');

		// If no default template ID set, set to ID of 0
		templateDefaultID = (config.templateDefault) ? config.templateDefault : 0;
		var templateDefaultRoute = config.templates[templateDefaultID].route;


		// Load default template
		loadTemplate(templateDefaultRoute);
	};




    /*
	 * loadTemplate
	 */
	var loadTemplate = function(templateRoute) {
		window.console && console.log('templateLoader.loadTemplate()');
		window.console && console.log('templateRoute: '+templateRoute);

		var templateFile;

		for( var i = 0; i < config.templates.length; i++ ){
			window.console && console.log('template '+i+':');
			window.console && console.log('config.templates[i].route: '+config.templates[i].route);

			if( config.templates[i].route == templateRoute ){
				templateFile = config.templates[i].file;
				window.console && console.log('templateFile: '+templateFile);
				break;
			}
		}

		if(templateFile){

			// Add loading class to prevent concurrent loading
			$('#template').addClass('loading');

			// Wait to load template until current template is hidden
			// If there is no current template loaded, the delay is 0
			// If there is current template, delay length of "hide" animation
			var ajaxDelay = $('#template').hasClass('empty') ? 0 : 0.4; 

			// Hide & unload current template
			TweenMax.to( $('#template'), 0.4, { opacity: 0, ease: Power2.easeOut });

			// Delayed ajax call
			TweenMax.delayedCall( ajaxDelay, function(){
				
				// Load new template HTML via ajax
				$.ajax({
		            url: templateFile,
		            type: 'GET',
		            dataType: "html",
		            success: function( templateHTML ){
		            	//window.console && console.log( 'templateHTML: '+templateHTML );

		            	// Append templateHTML
						$('#template').html(templateHTML);

						// Trigger resize
						$(window).trigger('resize');

		            	// Show new template
		            	TweenMax.to( $('#template'), .8, { opacity: 1, ease: Power2.easeOut });
		            },
		            error: function (xhr, ajaxOptions, thrownError) {
				    	window.console && console.log('thrownError: '+thrownError);
				    	window.console && console.log('xhr.status: '+xhr.status);
				    }
		        });

			});

		} else {
			window.console && console.log('No template found');
		}

	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('templateLoader.resizeHandler()');


	};


 	// Public
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'templateLoader',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('templateLoader.init()');

	    	initModule();
	    },

	    /*
		 * load
		 * Loads page templates into #template element by passing template name
		 */
	    load: function(templateRoute){
	    	window.console && console.log('templateLoader.load()');
	    	loadTemplate(templateRoute);
	    },


	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('templateLoader.resize()');
	    	resizeHandler();
	    }
	};
})();
