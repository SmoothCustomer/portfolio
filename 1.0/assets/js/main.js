// Global var for current page
if (typeof currentPage === 'undefined') {
    var currentPage = 'default';
}




// On Document Ready
$(window).load(function() {
	window.console && console.log('$(window).ready');

	// Pass current page name to app init so only the modules 
	// used for the current page are loaded
	app.init(currentPage);
});





/*
 * app module
 */
var app = (function(){

	// Private
	// ---------------------------------------------------------
	//var event = new Event('fontsloaded');
	var moduleMap, currentPage;
	var moduleList = [];


	// Define which modules will initialize for each page.
	// Init functions listed in array according to load order.
    setModuleMap = function() {
    	moduleMap = {
			"default": [ layout, mainUI, blocks, templateLoader, visualCodeHistory, videoHoverBlock ]
	    };
    };

    // Get list of modules to load for this page
	getModuleList = function() {
		//window.console && console.log('app.getModuleList()');
		
		/*
		if(!moduleMap[currentPage]){
			var moduleList = moduleMap['default'];
			currentPage = 'default';
		} else {
			var moduleList = moduleMap[currentPage];
		}
		*/

		// Need to check for modules loaded via ajax
		// after iniital page load and add to list?
		// or have method for adding a module to this list


	    return moduleList;
	};

	// Sets initial list of modules to load for this page
	setModuleList = function() {
		//window.console && console.log('app.setModuleList()');

		if(!moduleMap[currentPage]){
			var moduleList = moduleMap['default'];
			currentPage = 'default';
		} else {
			var moduleList = moduleMap[currentPage];
		}

		return moduleList;
	};


	// Initialize modules
	initModuleList = function(moduleList){
		window.console && console.log('app.initModuleList()');

    	for( var i=0; i < moduleList.length; i++ ) {
  			moduleList[i].init();
		}
	};


	/*
	 * arrayUnique
	 */
	arrayUnique = function(array){
		window.console && console.log('app.arrayUnique()');

	    var a = array.concat();
	    for(var i=0; i<a.length; ++i) {
	        for(var j=i+1; j<a.length; ++j) {
	            if(a[i] === a[j])
	                a.splice(j--, 1);
	        }
	    }
	    return a;
	};



	/*
	 * removeLoadedModules
	 */
	removeLoadedModules = function(modules){
	    window.console && console.log('app.removeLoadedModules()');


    };


    /*
	 * addLoadedModules
	 */
    addLoadedModules = function(modules){
    	window.console && console.log('app.addLoadedModules()');

    	var loadedModules = [];

   		// Format each module name as object
   		for ( var i = 0; i < modules.length; i++ ){
   			var module = window[modules[i]];
   			loadedModules.push(module);
   		}

		// Merges both arrays and gets unique items
		var newModuleList = arrayUnique( moduleList.concat(loadedModules) );
		moduleList = newModuleList;

		window.console && console.log('moduleList: '+JSON.stringify(moduleList));
    };


	/*
	 * Google Web Font Loader
	 * Loads fonts before calling initialization so that
	 * elements do not change size after fonts load.
	 */
	loadFonts = function(){
		window.console && console.log('app.loadFonts()');

		WebFont.load({

			typekit: {
				id: 'rol1wgl'
			},
			
    		/*
    		google: {
		      families: ['Muli:400,700']
		    },
		    
			custom: { 
				families: [	
					'ProximaNovaSemibold',
					'ProximaNovaRegular', 
					'ProximaNovaLight', 
					'ProximaNovaThin'
				], 
			  	urls: [		
			  		'assets/fonts/proximanova_semibold_macroman/stylesheet.css',
			  		'assets/fonts/proximanova_regular_macroman/stylesheet.css', 
					'assets/fonts/proximanova_light_macroman/stylesheet.css',
					'assets/fonts/proximanova_thin_macroman/stylesheet.css' 
				] 
			},

			*/
			
			loading: function() { 
				console.log('fonts loading'); 
			},
			fontloading: function(fontFamily, fontDescription) { 
				console.log(fontFamily+' loading'); 
			},
			fontactive: function(fontFamily, fontDescription) { 
				console.log(fontFamily+' active'); 
			},
			fontinactive: function(fontFamily, fontDescription) { 
				console.log(fontFamily+' inactive'); 
			},
			active: function() {
				//console.log('fonts load complete');
				$(window).trigger('fontsloaded');
			},
			inactive: function() {
				//console.log('fonts load failed');
				$(window).trigger('fontsloaded');
			}
		});
	};

	
	// Reveal page content on fontsloaded
	revealPage = function(){
		window.console && console.log('app.revealPage()');

		TweenMax.to($('#viewport'), 0.5, { opacity: 1, ease: Power2.easeOut });
	};
	 


	// Public
	// ---------------------------------------------------------
	return { 
		name: 'app',
	    init: function(page){
	    	window.console && console.log('app.init()');

	    	// Set current page
	    	currentPage = page;

	    	// Set moduleMap object
	    	// Arrays by page for which modules to initialize
	    	// has to be set after all modules are defined
	    	setModuleMap();

	    	// Google font loader
	    	loadFonts();
	    	
	    	// Listen for fonts to be finished loading
	    	// then initialize modules.
	    	$(window).on('fontsloaded', function(){
	    		//window.console && console.log('fontsloaded event');

	    		// Get list of modules to load for this page	    	
		    	moduleList = setModuleList();

		    	// Initialize modules
		    	initModuleList(moduleList);

		    	revealPage();
	    	});
	    },

	    // Checks to see if a module is 
	    // loaded for the current page
	    moduleLoaded: function(moduleName){
	    	//window.console && console.log('app.moduleLoaded()');

	  		for( var i = 0; i < moduleMap[currentPage].length; i++ ){
	  			if(moduleName == moduleMap[currentPage][i].name){
	  				return true;
	  			}
	  		}
	  		return false;
	    },
	    removeModules: function(modules){
	    	window.console && console.log('app.removeModules()');

	    	removeLoadedModules(modules);
	    },
	    addModules: function(modules){
	    	window.console && console.log('app.addModules()');

	    	addLoadedModules(modules);
	    },
	    getLoadedModules: function(){
	    	//window.console && console.log('app.getLoadedModules()');

	    	return getModuleList();
	    },
	    getCurrentPage: function(){
	    	//window.console && console.log('app.getCurrentPage()');
	    	return currentPage;
	    }

	};
})();

