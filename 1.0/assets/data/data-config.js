
/*
 * Site Config
 */


var config = {
	templateDefault: 0,
	templates: [
		{
			id: 0,
			name: 'home',
			file: 'templates/home/home.php',
			route: '/home'
		},
		{
			id: 1,
			name: 'ux',
			file: 'templates/ux/ux.php',
			route: '/ux'
		},
		{
			id: 1,
			name: 'design',
			file: 'templates/design/design.php',
			route: '/design'
		},
		{
			id: 1,
			name: 'code',
			file: 'templates/code/code.php',
			//file: 'templates/code/log-mike-ai/log-01/log-01.php',
			route: '/code'
		},
		{
			id: 1,
			name: 'me',
			file: 'templates/me/me.php',
			route: '/me'
		}
	]
};




