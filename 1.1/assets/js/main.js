
/*
 * JS App Framework
 */


/*
 * On window ready event
 */
$(window).load( function() {
	window.console && console.log('$(window).ready');
	
	$('body').addClass('js-app');

	// template name defined in template container attribute
	var page = $('.template').attr('data-template');

	if ( !page ) {
		var page = 'default';
	}

	// Pass currentPage to app initialization
	// Only the modules mapped to current page are initialized
	app.init(page);
});



/*
 * app module
 */
var app = (function(){

	// Private
	// ---------------------------------------------------------

	//var moduleMap;
	var currentPage;
	var moduleList = [];


	/*
	 * getModuleList
	 * Get list of modules to load for this page.
	 */
	getModuleList = function() {
		window.console && console.log('app.getModuleList()');
		
	    return moduleList;
	};


	/*
	 * setModuleList
	 * Sets initial list of modules to load for this page.
	 */
	setModuleList = function() {
		window.console && console.log('app.setModuleList()');

		var moduleList = [];

		// Push default modules to moduleList
		var defaultModules = config.defaultModules;
		var defaultLength = (defaultModules) ? defaultModules.length : 0;
		if (defaultLength > 0){
			for( var i = 0; i < defaultLength; i++ ) {
				moduleList.push( defaultModules[i] );
			}
		}



		window.console && console.log('currentPage: '+currentPage);
		// Push currentPage modules to moduleList
		var currentTemplate = (currentPage == 'default') ? config.templateDefault : currentPage;
		var currentPageModules = config.templates[currentTemplate].modules;
		var currentPageLength = currentPageModules.length;

		if (currentPageLength > 0){
			for( var i = 0; i < currentPageLength; i++ ) {
				moduleList.push( currentPageModules[i] );
			}
		}
		window.console && console.log('moduleList: ' + JSON.stringify(moduleList));


		return moduleList;
	};




	/*
	 * initModuleList
	 */
	initModuleList = function(){
		window.console && console.log('app.initModuleList()');

    	for( var i=0; i < moduleList.length; i++ ) {
			window.console && console.log('initializing: ' + moduleList[i].name);
  			moduleList[i].init();
		}
	};



	// Public
	// ---------------------------------------------------------
	return { 
		name: 'app',

		/*
		 * init
		 */
	    init: function(page){
	    	window.console && console.log('app.init()');

	    	// Set current page
	    	currentPage = page;

			// Get list of modules to load for this page	    	
		    moduleList = setModuleList();
			
			// Initialize core modules defined in data-config.js
			for( var i=0; i < config.coreModules.length; i++ ) {
				window.console && console.log('initializing: ' + config.coreModules[i].name);
  				config.coreModules[i].init();
			}

			// Google font loader
	    	fonts.load();

	    	// Listen for fonts to be finished loading 
			// then initialize modules & reveal page.
	    	$(window).on('fontsloaded', function(){
	    		window.console && console.log('fontsloaded event');
				window.console && console.log('fontsloaded event');

				if (!layout.isPageRevealed()) {
					layout.revealPage();
				}
	    	});
	    },

		/*
		 * loadModules
		 */
	    loadModules: function(){
	    	window.console && console.log('app.loadModules()');

	    	// Initialize modules
		    initModuleList();

	    },

		/*
		 * updateModuleList
		 */
	    updateModuleList: function(){
	    	window.console && console.log('app.updateModuleList()');

	    	// Initialize modules
		    moduleList = setModuleList();

	    },

		/*
		 * getLoadedModules
		 */
	    getLoadedModules: function(){
	    	window.console && console.log('app.getLoadedModules()');

	    	return getModuleList();
	    }
		
	};
})();


