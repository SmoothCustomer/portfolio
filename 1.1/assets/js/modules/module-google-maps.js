/*
 * googleMaps module
 */
var googleMaps = (function () {


 	// Private Vars
 	// ---------------------------------------------------------

    var map;



    // Private Methods
 	// ---------------------------------------------------------

	/*
	 * initModule
	 */
    var initModule = function () {
        window.console && console.log('googleMaps.initModule()');

        loadGoogleMapScript();
    };


    /*
 	 * loadGoogleMapScript
     */
    var loadGoogleMapScript = function() {
        window.console && console.log('googleMaps.loadGoogleMapScript()');

        var mapScriptURL = mapURL = 'https://maps.googleapis.com/maps/api/js?sensor=true&key=AIzaSyATpjLBxT2WHQjMefX-HfOXPr0_uFS64zQ&callback=mapScriptLoaded';
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = mapScriptURL;
        document.body.appendChild(script);
    };


    /*
     * Google API callback
     */
    window.mapScriptLoaded = function () {
        window.console && console.log('window.mapScriptLoaded()');

        initMap();
        $(window).trigger("googleLoaded");
    };


	/*
	 * initMap
	 */
    var initMap = function() {
        window.console && console.log('googleMaps.initMap()');

        var color1 = '#804033'; // #804033"
        //var centerLoc = new google.maps.LatLng( geoLat, geoLng );

        var mapOptions = {
            scrollwheel: false,
            navigationControl: true,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true,
            zoomControl: true,
            clickableIcons: false,
            center: { lat: 33.669775, lng: -117.994198 },
            zoom: 12,
            disableDefaultUI: false,
            backgroundColor: '#ffffff'
            //styles: mapStyles // defined in data-config.js
        }

        // Set map var
        map = new google.maps.Map(document.getElementById('map'), mapOptions);


        // Overlay
        PhotoMarker.prototype = new google.maps.OverlayView();

        PhotoMarker.prototype.draw = function () {
            window.console && console.log(' PhotoMarker.prototype.draw()');

            var me = this;
            var div = this.div_;

            if (!div) {
                window.console && console.log('div does not exists');

                var markerHTML;
                markerHTML += '<div class="location">';
                markerHTML += '<img src="' + this.photoURL + '" alt="location"/>';
                markerHTML += '</div>';

                var marker = $('<div class="location"><img src="' + this.photoURL + '" alt="location"></div>')[0];

                div = this.div_ = marker;

                // Then add the overlay to the DOM
                var panes = this.getPanes();
                panes.overlayImage.appendChild(div);

            } else {
                window.console && console.log('div does exists');
            }

            // Position the overlay 
            var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
            if (point) {
                div.style.left = point.x + 'px';
                div.style.top = point.y + 'px';
            }
        };

        PhotoMarker.prototype.remove = function () {
            // Check if the overlay was on the map and needs to be removed.
            if (this.div_) {
                this.div_.parentNode.removeChild(this.div_);
                this.div_ = null;
            }
        };

        PhotoMarker.prototype.getPosition = function () {
            return this.latlng_;
        };

    };



    function PhotoMarker(latlng, map, photoURL) {
        this.latlng_ = latlng;
        this.photoURL = photoURL;

        /*Do this or nothing will happen:*/
        this.setMap(map);
    }




    /*
	 * addPhotoMarker
	 */
    var addPhotoMarker= function (lat, lng, photoURL) {
        window.console && console.log('googleMaps.addPhotoMarker()');
        window.console && console.log('lat: ' + lat);
        window.console && console.log('lng: ' + lng);
        window.console && console.log('photoURL: ' + photoURL);


        /* Add the overlay to your map */
        var latLng = new google.maps.LatLng(lat, lng);
        var marker = new PhotoMarker(latLng, map, photoURL);
    };



	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
        window.console && console.log('googleMaps.resizeHandler()');

	};



	// Public Methods
	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
        name: 'googleMaps',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
            window.console && console.log('googleMaps.init()');

	    	initModule();
        },

	    /*
		 * loadMapScript
		 */
        loadMapScript: function () {
            window.console && console.log('googleMaps.loadMapScript()');

            loadGoogleMapScript();
        },

        /*
		 * addPhotoMarkerAtLocation
		 */
        addPhotoMarkerAtLocation: function (lat, lng, photoURL) {
            window.console && console.log('googleMaps.addPhotoMarkerAtLocation()');

            addPhotoMarker(lat, lng, photoURL);
        },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('googleMaps.resize()');

	    	resizeHandler();
	    }
	};
})();