/*
 * instagram module
 */
var instagram = (function () {


    // Private Vars
    // ---------------------------------------------------------



    // Private Methods
    // ---------------------------------------------------------


	/*
	 * initModule
	 */
    var initModule = function () {
        window.console && console.log('instagram.initModule()');

        $(window).on('googleLoaded', function () {
            getPhotos();
        });
        
    };


	/*
	 * getPhotos
	 */
    var getPhotos = function () {
        window.console && console.log('instagram.getPhotos()');


        
        var token = '20275559.dc66281.7be980ce584d445c8deb8a2d8a268d65', // learn how to obtain it below
            userid = 20275559, // User ID - get it in source HTML of your Instagram profile or look at the next example :)
            num_photos = 10; // how much photos do you want to get
            lat = 33.669775,
            lng = -117.994198;

        //var url = 'https://api.instagram.com/v1/users/' + userid + '/media/recent';
        //var url = 'https://api.instagram.com/v1/media/search?lat=' + lat+'&lng='+lng;
        // https://api.instagram.com/v1/media/{media-id}

        var url = 'https://api.instagram.com/v1/users/self/media/liked';



        $.ajax({
            url: url, // or /users/self/media/recent for Sandbox
            dataType: 'jsonp',
            type: 'GET',
            data: { access_token: token, count: num_photos },
            success: function (data) {

                //console.log(data);
                window.console && console.log(data);

                if (data) {
                    window.console && console.log('data exists');
                    window.console && console.log('data.data.length: ' + data.data.length);

                    for ( var i = 0; i < data.data.length; i++) {
                        window.console && console.log('id: ' + data.data[i].id);

                        if (data.data[i].location) {
                            var lat = data.data[i].location.latitude;
                            var lng = data.data[i].location.longitude;
                            var photoURL = data.data[i].images.low_resolution.url;
                            googleMaps.addPhotoMarkerAtLocation(lat, lng, photoURL);
                        }
                        
                    }
                }

            },
            error: function (data) {
                console.log(JSON.stringify(data)); // send the error notifications to console
            }
        });
        

    };


	/*
	 * resizeHandler
	 */
    var resizeHandler = function () {
        window.console && console.log('instagram.resizeHandler()');

    };


    // Public Methods
    // ---------------------------------------------------------

    return {

        // Module name id for dynamic initialization
        name: 'instagram',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
        init: function () {
            window.console && console.log('instagram.init()');

            initModule();
        },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
        resize: function () {
            //window.console && console.log('instagram.resize()');

            resizeHandler();
        }
    };
})();
