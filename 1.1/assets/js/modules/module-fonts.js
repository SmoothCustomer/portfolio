/*
 * fonts module
 */
var fonts = (function () {
 

 	// Private
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	var fontLoadingComplete = false;


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('fonts.initModule()');

	};


	/*
	 * Google Web Font Loader
	 * Loads fonts before calling initialization so that
	 * elements do not change size after fonts load.
	 */
	loadFonts = function(){
		window.console && console.log('fonts.loadFonts()');

    	WebFont.load({
    		typekit: {
				id: 'rol1wgl'
			},
			loading: function() { 
				console.log('fonts loading'); 
			},
			fontloading: function(fontFamily, fontDescription) { 
				console.log(fontFamily+' loading'); 
			},
			fontactive: function(fontFamily, fontDescription) { 
				console.log(fontFamily+' active'); 
			},
			fontinactive: function(fontFamily, fontDescription) { 
				console.log(fontFamily+' inactive'); 
			},
			active: function() {
				//console.log('fonts load complete');
				fontLoadingComplete = true;
				$(window).trigger('fontsloaded');
			},
			inactive: function() {
				//console.log('fonts load failed');
				fontLoadingComplete = true;
				$(window).trigger('fontsloaded');
			}
		});
	};



	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('fonts.resizeHandler()');

	};


 	// Public
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'fonts',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('fonts.init()');
	    	initModule();
	    },

		/*
		 * load
		 */
	    load: function(){
	    	window.console && console.log('fonts.load()');
	    	loadFonts();
	    },

		/*
		 * isFontLoadingComplete
		 */
	    isFontLoadingComplete: function(){
	    	window.console && console.log('fonts.isFontLoadingComplete()');
	    	return fontLoadingComplete;
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	window.console && console.log('fonts.resize()');
	    	resizeHandler();
	    }
	};
})();
