/*
 * blocks module
 */
var blocks = (function () {
 
 	// Private
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('blocks.initModule()');

	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('blocks.resizeHandler()');

	};


 	// Public
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'blocks',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('blocks.init()');
	    	initModule();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	window.console && console.log('blocks.resize()');
	    	resizeHandler();
	    }
	};
})();
