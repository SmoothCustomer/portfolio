/*
 * carousel module
 */
var carousel = (function () {
 
 	// Private
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('carousel.initModule()');

	};

	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('carousel.resizeHandler()');

	};


 	// Public
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'carousel',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function (){
	    	window.console && console.log('carousel.init()');
	    	initModule();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function (){
	    	window.console && console.log('carousel.resize()');
	    	resizeHandler();
	    }
	};
})();
