/*
 * toolbox module
 */
var toolbox = (function () {
 
 	// Private
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('toolbox.initModule()');

	};


	/*
	 * createUniqueArray
	 * NOTE: Analyze efficiency, looks almost like N^2, could maybe make N log N with binary search for match/push method
	 */
	createUniqueArray = function(array){
		window.console && console.log('toolbox.createUniqueArray()');

	    var uniqueArray = array.concat();
	    for ( var i = 0; i < uniqueArray.length; ++i ) {
	        for ( var j = i + 1; j < uniqueArray.length; ++j ) {
				if ( uniqueArray[i] === uniqueArray[j] ) {
					uniqueArray.splice( j--, 1 );
				} 
	        }
	    }
	    return uniqueArray;
	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('toolbox.resizeHandler()');

	};


 	// Public
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'toolbox',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function (){
	    	window.console && console.log('toolbox.init()');
	    	initModule();
	    },

		/*
		 * uniqueArray
		 */
		uniqueArray: function (array) {
			window.console && console.log('toolbox.uniqueArray()');
			return createUniqueArray(array);
		},

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function (){
	    	window.console && console.log('toolbox.resize()');
	    	resizeHandler();
	    }
	};
})();
