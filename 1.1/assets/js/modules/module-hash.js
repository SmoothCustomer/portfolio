/*
 * hash module
 * 
 */
var hash = (function () {
 
 	// Private
 	// ---------------------------------------------------------


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('hash.initModule()');

		// Initial page load hash change
		handleHashChange();

		// Hash change events
		initHashEvents();
	};


	/*
	 * setHash
	 */
	var setHash = function(route) {
		window.console && console.log('hash.setHash()');

		parent.location.hash = route;
	};


	/*
	 * getCurrentHash
	 */
	var getCurrentHash = function(route) {
		window.console && console.log('hash.getCurrentHash()');

		var currentHash = window.location.hash.substr(1);
		return currentHash;
	};


	/*
	 * initHashEvents
	 */
	var initHashEvents = function(route) {
		window.console && console.log('hash.initHashEvents()');

		window.removeEventListener( "hashchange", function(){}, false);
		window.addEventListener( "hashchange", function(){
			handleHashChange();
		}, false);
	};

	/*
	 * handleHashChange
	 */
	var handleHashChange = function(route) {
		window.console && console.log('hash.handleHashChange()');

		// Get currentHash on page load
		// If empty load default template
		// If hash has value, compare to routes defined in config.templates
		// If match is found, load that template
		// If match not found, load default template
		var currentHash = getCurrentHash();
		window.console && console.log('currentHash: ' + currentHash);

		if (currentHash != '') {
			var foundTemplate = false;
			for ( var key in config.templates ) {
				if ( config.templates.hasOwnProperty(key) ) {
					if ( currentHash == config.templates[key].route ) {
						templateLoader.load(key);
						foundTemplate = true;
						break;
					}
				}
			}
			if (!foundTemplate) {
				templateLoader.load('default');
				setHash(config.templates[config.templateDefault].route);
			}

		} else {
			window.console && console.log('hash empty load default template');
			templateLoader.load('default');
			setHash(config.templates[config.templateDefault].route);
		}

	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('hash.resizeHandler()');

	};


 	// Public
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'hash',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('hash.init()');

	    	initModule();
	    },

		set: function(route) {
			window.console && console.log('hash.set()');

			setHash(route);
		},

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('hash.resize()');
	    	resizeHandler();
	    }
	};
})();
