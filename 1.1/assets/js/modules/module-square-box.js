/*
 * squareBox module
 * tag format: <div class="square-box" data-inner-padding="30px"></div>
 */
var squareBox = (function () {
 
 	// Private
 	// ---------------------------------------------------------


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('squareBox.initModule()');

		sizeSquareBoxes();
	};


	/*
	 * sizeSquareBoxes
	 */
	var sizeSquareBoxes = function() {
		window.console && console.log('squareBox.sizeSquareBoxes()');


		$('.square-box').each( function() {
			
			var width = $(this).outerWidth();
			var innerPadding = ($(this).attr('data-inner-padding')) ? parseInt($(this).attr('data-inner-padding'), 10) : 0;
			var height, innerWrapperHeight;
			
			// Set height based on defined aspect ratio defined in classes
			// square-box (default): height = width
			// square-box-half: height = width/2;
			// square-box-cinema: height =  width/(16/9)

			// square-box-half
			if ($(this).hasClass('square-box-half')) {
				innerWrapperHeight = (width - (innerPadding * 2)) / 2;
				//height = width / 2;
				
			// square-box-cinema
			} else if ($(this).hasClass('square-box-cinema')) {
				innerWrapperHeight = (width - (innerPadding * 2)) / (16/9);
				//height = width / (16/9);

			// square-box-widescreen
			} else if ($(this).hasClass('square-box-widescreen')) {
				innerWrapperHeight = (width - (innerPadding * 2)) / 1.6;
				//height = width / 1.6;

			// default			
			} else {
				innerWrapperHeight = (width - (innerPadding * 2));

			}


			height = innerWrapperHeight + (innerPadding * 2);

			// Calculate inner wrapper dimensions
			//var innerWrapperHeight = height - (innerPadding * 2);
			var innerWrapperWidth = width - (innerPadding * 2);



			// Set height			
			$(this).css({
				'height': height
			});
			
			// If no inner wrapper add one
			if ($(this).find('.square-box-inner-wrapper').length <= 0) {
				$(this).wrapInner( "<div class='square-box-inner-wrapper'></div>");
			}

			// Set inner wrapper dimensions
			$(this).find('.square-box-inner-wrapper').css({
				'height': innerWrapperHeight,
				'width': innerWrapperWidth,
				'left': innerPadding,
				'top': innerPadding
			});
		
		});
	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('squareBox.resizeHandler()');

		sizeSquareBoxes();
	};


 	// Public
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'squareBox',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('squareBox.init()');

	    	initModule();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('squareBox.resize()');
	    	resizeHandler();
	    }
	};
})();
