/*
 * hoverBox module
 * tag format: <div class="hover-box" data-video-url="" data-image-backup-url="images/fpo-promo-1.jpg"></div>
 */
var hoverBox = (function () {
 
 	// Private
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	var mouseOverScale = 1.00;
	var mouseOverScaleSpeed = 0.6;
	var mouseOutScaleSpeed = 0.6;
	var backgroundScaleMin = 1.00;


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('hoverBox.initModule()');

		createHoverBoxElements();
		createHoverBoxEvents();
	};


	/*
	 * createHoverBoxElements
	 */
	var createHoverBoxElements = function() {
		window.console && console.log('hoverBox.createHoverBoxElements()');

		$('.hover-box').each( function(index) {

			var imageURL = $(this).attr('data-image-backup-url');
			var videoURL = $(this).attr('data-video-url');


			// Add preloader
			var preloader = '';
			preloader += '<div class="pre-loader">';
				preloader += '<svg class="spinner">';
					preloader += ' <circle cx="20" cy="20" r="18"></circle>';
				preloader += '</svg>';
			preloader += '</div>';
			$(this).append(preloader);


			
			// If video defined, don't load backup image
				// Load video, on canplaythrough, fade in video, hide preloader
					// If video load error, load backup image
			// If no video, load backupimage
			
			// If image defined, load backup image
				// add event for asset load
					// on image load, fade in image, hide preloader

			if (videoURL) {
				createHoverBoxVideo( $(this), index, videoURL, imageURL );

			} else {
				createHoverBoxImage( $(this), imageURL );
			}



			//destroyPreloader($(this));

			// Add backup image
			 
		});

	};


	/*
	 * destroyPreloader
	 */
	var destroyPreloader = function(hoverBox) {
		//window.console && console.log('hoverBox.destroyPreloader()');
	
		hoverBox.find('.pre-loader').css('opacity', 0);
	};


	/*
	 * createHoverBoxVideo
	 */
	var createHoverBoxVideo = function(hoverBox, index, videoURL, imageURL) {
		window.console && console.log('hoverBox.createHoverBoxVideo()');
	
		if ( videoURL && !client.mobile ) {

			var videoElementHTML = '';
			videoElementHTML += '<div class="video">';
  			videoElementHTML += '<video id="video-'+index+'" class="video" playsinline muted loop>';
    		videoElementHTML += '<source src="' + videoURL + '" type="video/mp4">';
			videoElementHTML += '</video>';
			videoElementHTML += '</div>';
			hoverBox.append(videoElementHTML);
			hoverBox.addClass('video-available');

			// canplaythrough event adds playable class to video reveals video
			var videoObject = hoverBox.find('video').get(0);
			videoObject.containerID = '#video-' + index;

			// Video load event
			videoObject.removeEventListener('canplaythrough', function handleCanPlay(event){});
			videoObject.addEventListener('canplaythrough', function handleCanPlay(event){

				var videoObject = event.target;

				if (!$(videoObject.containerID).hasClass('playable')) {
					window.console && console.log('hoverBox video canplaythrough');

					destroyPreloader(hoverBox);

					// Add playable class
					$(videoObject.containerID).addClass('playable');

					// Reveal video
					TweenMax.to( hoverBox.find('.video'), 0, { scale: backgroundScaleMin });
					TweenMax.to( hoverBox.find('.video'), mouseOverScaleSpeed, { opacity: 1, ease: Power2.easeOut });
				}
			});
		}
	};


	/*
	 * createHoverBoxImage
	 */
	var createHoverBoxImage = function(hoverBox, imageURL) {
		//window.console && console.log('hoverBox.createHoverBoxImage()');

		if ( imageURL ) {
			var image = new Image;
			image.onload = function() {
				//window.console && console.log('hoverBox image loaded');
				destroyPreloader(hoverBox);

				var imageBackupElement = '<div class="image-backup full-frame-image" style="background-image: url(' + imageURL + ');"></div>';
				hoverBox.append(imageBackupElement);
				hoverBox.addClass('image-backup-available');

				TweenMax.to( hoverBox.find('.image-backup'), 0, { scale: backgroundScaleMin });
				TweenMax.to( hoverBox.find('.image-backup'), mouseOverScaleSpeed, { opacity: 1, ease: Power2.easeOut });
			};
			image.onerror = function() {
				window.console && console.log('error loading image: ' + imageURL);
			};
			image.src = imageURL;
		}
	};


	/*
	 * createHoverBoxEvents
	 */
	var createHoverBoxEvents = function() {
		window.console && console.log('hoverBox.createHoverBoxEvents()');
	
		// Mouseenter
		$('.hover-box').unbind( 'mouseenter' );
		$('.hover-box').on( 'mouseenter', function() {
			handleHoverBoxMouseEnter($(this));
		});

		// Mouseleave
		$('.hover-box').unbind( 'mouseleave' );
		$('.hover-box').on( 'mouseleave', function() {
			handleHoverBoxMouseLeave($(this));
		});

	};



	/*
	 * handleHoverBoxMouseEnter
	 */
	var handleHoverBoxMouseEnter = function(box) {
		window.console && console.log('hoverBox.handleHoverBoxMouseEnter()');

		var imageAvailable = box.hasClass('image-backup-available');
		var videoAvailable = box.hasClass('video-available');
		var videoPlayable = box.find('video').hasClass('playable');
		var scale = (box.attr('data-hover-scale')) ? box.attr('data-hover-scale') : mouseOverScale;


		// Play video on non-mobile devices
		if (!client.mobile && videoAvailable && videoPlayable) {
			var videoObject = box.find('video').get(0);
			videoObject.play();
		}


		TweenMax.to( box.find('.image-backup'), mouseOverScaleSpeed, { scale: scale, ease: Power2.easeOut });
		TweenMax.to( box.find('.video'), mouseOverScaleSpeed, { scale: scale, ease: Power2.easeOut });
	};


	/*
	 * handleHoverBoxMouseLeave
	 */
	var handleHoverBoxMouseLeave = function(box) {
		window.console && console.log('hoverBox.handleHoverBoxMouseLeave()');

		var imageAvailable = box.hasClass('image-backup-available');
		var videoAvailable = box.hasClass('video-available');
		var videoPlayable = box.find('video').hasClass('playable');


		// Pause video on non-mobile devices
		if (!client.mobile && videoAvailable && videoPlayable) {
			var videoObject = box.find('video').get(0);
			videoObject.pause();
		}


		TweenMax.to( box.find('.image-backup'), mouseOutScaleSpeed, { scale: backgroundScaleMin, ease: Power2.easeOut });
		TweenMax.to( box.find('.video'), mouseOutScaleSpeed, { scale: backgroundScaleMin, ease: Power2.easeOut });
	};




	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		//window.console && console.log('hoverBox.resizeHandler()');

	};



 	// Public
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'hoverBox',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('hoverBox.init()');
	    	initModule();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('hoverBox.resize()');
	    	resizeHandler();
	    }
	};
})();
