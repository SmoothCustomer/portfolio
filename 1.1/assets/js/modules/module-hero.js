/*
 * hero module
 * 
 */
var hero = ( function() {
 
 	// Private
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('hero.initModule()');

	};

	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('hero.resizeHandler()');

	};


 	// Public
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'hero',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function() {
	    	window.console && console.log('hero.init()');
	    	initModule();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function() {
	    	window.console && console.log('hero.resize()');
	    	resizeHandler();
	    }
	};
})();