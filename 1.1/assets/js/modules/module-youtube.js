/*
 * youTube module
 * 
 * tag format: <div class="youtube-video" data-youtube-id="YOUTUBE_VIDEO_ID" data-width="480" data-height="360"></div>
 * Add this HTML anywhere on page, module will add videos to defined area, and reveal on load.
 */
var youTube = ( function() {
 
 	// Private
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	var youTubeVideos = [];


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('youTube.initModule()');

		// YouTube API loads asynchronously
		if (youTubeAPILoaded) {
			initVideos();
		} else {
			$(window).on( 'youTubeReady', function() {
				$(window).unbind( "youTubeReady" );
				initVideos();
			});
		}
	};


	/*
	 * initVideos
	 */
	var initVideos = function() {
		window.console && console.log('youTube.initVideos()');

		$('.youtube-video').each ( function(index) {
			var playerID = 'player' + index;
			var videoID = $(this).attr('data-youtube-id');
			$(this).attr( 'id', playerID );
			addVideo( playerID, videoID );
		});

		
	};


	/*
	 * addVideo
	 */
	var addVideo = function( playerID, videoID ) {
		window.console && console.log('youTube.addVideo()');

		var player = new YT.Player( playerID, {
			height: '383',
			width: '680',
			videoId: videoID,
			playerVars: {
				'rel': 0,
				'showinfo': 0,
				'autohide': 1
			}
        });

        // Store all players in associative array
        // so each video player can be accessed by id
		youTubeVideos[playerID] = player;
		$('#'+playerID).attr( 'wmode', 'Opaque' );
	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('youTube.resizeHandler()');

	};


 	// Public
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'youTube',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function() {
	    	window.console && console.log('youTube.init()');
	    	initModule();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function() {
	    	window.console && console.log('youTube.resize()');
	    	resizeHandler();
	    }
	};
})();



// Asynchronous YouTube API Loading
// ---------------------------------------------------------

/*
 * YouTubeAPI
 * YouTube javascript api loads asynchronously, and callback has to be
 * in the global scope which is why this initialization is not contained
 * within a module. 
 */
var youTubeAPILoaded = false;

/*
 * onYouTubeIframeAPIReady
 */
function onYouTubeIframeAPIReady() {
	window.console && console.log('onYouTubeIframeAPIReady()');

	$(window).trigger('youTubeReady');
	youTubeAPILoaded = true;
};

/*
 * loadYouTubeAPI
 */
var loadYouTubeAPI = function() {
	window.console && console.log('loadYouTubeAPI()');

	// This loads the IFrame YouTube Player API code asynchronously
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
};

loadYouTubeAPI();
