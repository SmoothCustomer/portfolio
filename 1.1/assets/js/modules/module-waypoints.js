/*
 * waypoints module
 */
var waypoints = (function () {
 
 	// Private
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

 	var triggerOffset = 0.95;
 	var speed = 0.5;
	//var stagger = 0.15;
	var stagger = 0.25;

	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('waypoints.initModule()');

		// Hide and pre-position waypoints for animation
		$('.waypoint').each( function(index){
			TweenMax.to( $(this), 0, { y: 80, opacity: 0 });
		});

	};


	/*
	 * checkWaypoints
	 */
	var checkWaypoints = function() {
		window.console && console.log('waypoints.checkWaypoints()');

		var waypointArray = [];
		var viewportHeight = layout.getViewportHeight();

		$('.waypoint').each( function(index){
			var waypoint = $(this);
			if( !waypoint.hasClass('triggered') ){
				waypointArray.push(waypoint);
			}
		});

		for (var i = 0; i < waypointArray.length; i++ ){
			var delay = i * stagger;
			var waypoint = waypointArray[i];
			var waypointDistanceFromTop = waypoint.offset().top - $(window).scrollTop();
			var offset = waypointDistanceFromTop/viewportHeight;

			if(offset <= triggerOffset){
				TweenMax.to( waypoint, 0, { y: 80, opacity: 0, ease: Power2.easeOut });
				TweenMax.to( waypoint, speed, { y: 0, opacity: 1, ease: Power2.easeOut, delay: delay });
				waypoint.addClass('triggered');
			}
		}

	};


	/*
	 * animationSpeed
	 */
    var animationSpeed = function(){
    	window.console && console.log('waypoints.animationSpeed()');

    	return speed;
    };


	/*
	 * scrollHandler
	 */
	var scrollHandler = function() {
		window.console && console.log('waypoints.scrollHandler()');

		checkWaypoints();
	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('waypoints.resizeHandler()');

		checkWaypoints();
	};


 	// Public
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'waypoints',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
			window.console && console.log('waypoints.init()');

	    	initModule();
	    },

	    /*
		 * getAnimationSpeed
		 */
	    getAnimationSpeed: function(){
	    	window.console && console.log('waypoints.getAnimationSpeed()');

	    	//return animationSpeed();
	    	return 0.6;
	    },

	    /*
		 * scroll
		 * Called by layout module on viewport scroll event
		 */
	    scroll: function(){
	    	//window.console && console.log('mainUI.resize()');
	    	scrollHandler();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('waypoints.resize()');
	    	resizeHandler();
	    }
	};
})();
