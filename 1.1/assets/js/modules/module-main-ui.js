/*
 * mainUI module
 */
var mainUI = (function () {
 
 	// Private
 	// ---------------------------------------------------------


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('mainUI.initModule()');

		initRouteLinks();
		initMainNav();
	};


	/*
	 * initMainNav
	 */
	var initMainNav = function() {
		window.console && console.log('mainUI.initMainNav()');
	
	};


	/*
	 * initRouteLinks
	 */
	var initRouteLinks = function() {
		window.console && console.log('mainUI.initRouteLinks()');

		$('a.route').unbind('click');
    	$('a.route').on('click', function(event){
    		window.console && console.log('.a.route click');

    		if( $(this).hasClass('route') ){
    			event.preventDefault();
    			var id = $(this).attr('data-id');
				var route = $(this).attr('href');
				hash.set(route);
    			templateLoader.load(id);
    		}
    	});
	};



	/*
	 * scrollHandler
	 */
	var scrollHandler = function() {
		window.console && console.log('mainUI.scrollHandler()');

		// Collapse/Expand main nav on scroll
		var scrollTop = $(window).scrollTop();

		if (scrollTop > 0 && !$('#masthead').hasClass('collapsed')) {
			$('#masthead').addClass('collapsed');

			TweenMax.to( $('#masthead'), 0.4, { height: 100, ease: Power2.easeOut });
			TweenMax.to( $('#main-ui'), 0.4, { height: 135, top: -35, ease: Power2.easeOut });
			TweenMax.to( $('#main-ui-background'), 0.4, { opacity: 0.8, ease: Power2.easeOut });

		} else if( scrollTop <= 0 && $('#masthead').hasClass('collapsed') ) {
			$('#masthead').removeClass('collapsed');

			TweenMax.to( $('#masthead'), 0.4, { height: 180, ease: Power2.easeIn });
			TweenMax.to( $('#main-ui'), 0.4, { height: 180, top: 0, ease: Power2.easeOut });
			TweenMax.to( $('#main-ui-background'), 0.4, { opacity: 0, ease: Power2.easeOut });
		}
	};



	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('mainUI.resizeHandler()');

	};


 	// Public
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'mainUI',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('mainUI.init()');

	    	initModule();
	    },

	    /*
		 * scroll
		 * Called by layout module on viewport scroll event
		 */
	    scroll: function(){
	    	//window.console && console.log('mainUI.resize()');
	    	scrollHandler();
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('mainUI.resize()');
	    	resizeHandler();
	    }
	};
})();
