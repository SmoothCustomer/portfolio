/*
 * templateLoader module
 * 
 * NOTE: Add URL Hash functionality to this module. Also handle deep linking
 */
var templateLoader = (function () {
 
 	// Private
 	// ---------------------------------------------------------

 	var templateDefaultID;
	var templateLoaded = false;
	var currentTemplateID;


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('templateLoader.initModule()');

		// If no default template ID set, set to ID of 0
		//templateDefaultID = (config.templateDefault) ? config.templateDefault : 'home';

		// Load default template
		//loadTemplate(templateDefaultID);
	};




    /*
	 * loadTemplate
	 */
	var loadTemplate = function( templateID ) {		
		window.console && console.log('-------------------------');
		window.console && console.log('templateLoader.loadTemplate()');
		
		// If template already loaded do not reload
		if (templateID != currentTemplateID) {

			currentTemplateID = templateID;

			// Get default template if 'default' is passed as argument
			if ( templateID == 'default' ) {
				templateID = config.templateDefault;
			}
			var templateFile = (config.templates[templateID].file);


			if( templateFile ){

				// Initialize modules after template is loaded
				app.updateModuleList();

				// Add loading class to prevent concurrent loading
				$('#template').addClass('loading');

				// Wait to load template until current template is hidden
				// If there is no current template loaded, the delay is 0
				// If there is current template, delay length of "hide" animation
				var ajaxDelay = $('#template').hasClass('empty') ? 0 : 0.4; 

				// Hide & unload current template
				TweenMax.to( $('#template'), 0.4, { opacity: 0, ease: Power2.easeOut });

				// Delayed ajax call
				TweenMax.delayedCall( ajaxDelay, function(){
				
					// Load new template HTML via ajax
					$.ajax({
						url: templateFile,
						type: 'GET',
						dataType: "html",
						success: function( templateHTML ){
		            		//window.console && console.log( 'templateHTML: '+templateHTML );

							templateLoaded = true;

							// Move page to top scroll position
							$(window).scrollTop(0);

		            		// Append templateHTML
							$('#template').html(templateHTML);

							// Trigger resize
							$(window).trigger('resize');

							// Initialize modules after template is loaded
							app.loadModules();

		            		// Show new template
		            		TweenMax.to( $('#template'), .8, { opacity: 1, ease: Power2.easeOut });
						},
						error: function (xhr, ajaxOptions, thrownError) {
				    		window.console && console.log('thrownError: '+thrownError);
				    		window.console && console.log('xhr.status: '+xhr.status);
						}
					});

				});

			} else {
				window.console && console.log('No template found');
			}

		}

	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function() {
		window.console && console.log('templateLoader.resizeHandler()');


	};


 	// Public
 	// ---------------------------------------------------------

	return {

		// Module name id for dynamic initialization
		name: 'templateLoader',

		/*
		 * init
		 * Main init function called when module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('templateLoader.init()');

	    	initModule();
	    },

	    /*
		 * load
		 * Loads page templates into #template element by passing template name
		 */
	    load: function(templateRoute){
	    	window.console && console.log('templateLoader.load()');
	    	loadTemplate(templateRoute);
	    },

		/*
		 * isTemplateLoaded
		 */
	    isTemplateLoaded: function(){
	    	window.console && console.log('templateLoader.isTemplateLoaded()');
			return templateLoaded;
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('templateLoader.resize()');
	    	resizeHandler();
	    }
	};
})();
