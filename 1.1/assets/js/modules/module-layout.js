/*
 * layout module
 */
var layout = (function () {
 
 	// Private
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	var pageRevealed = false;
	var viewportWidth, viewportHeight;
	var layoutType, layoutOrientation;
	var orientationTimer, resizeTimer;
	var resizeDelay = 0;
	var orientationDelay = 200;
	var loadedModules;
	var breakPoint1 = 768;
	var breakPoint2 = 992;
	var breakPoint3 = 1200;


	/*
	 * initModule
	 */
	var initModule = function() {
		window.console && console.log('layout.initModule()');

		// Prevent bounce effect on iOS
    	document.ontouchmove = function(event){
	    	event.preventDefault();
		}
		initResize();
		initScroll();
	};


	/*
	 * updateDimensions
	 * Set browser dimensions.
	 */
	var updateDimensions = function() {
		window.console && console.log('layout.updateDimensions()');

		document.body.style.overflow = "hidden";
		viewportWidth = $(window).width();
		viewportHeight = $(window).height();
		document.body.style.overflow = "";
	};


	/*
	 * updateLayoutType
	 * Sets layout size tags. Based on Bootstrap v3.3.7 responsive breakpoints 
	 */
	var updateLayoutType = function() {
		//window.console && console.log('layout.updateLayoutType()');

		var currentLayoutType = layoutType;

		if (viewportWidth > breakPoint3) {
			layoutType = 'lg';

		} else if (viewportWidth > breakPoint2) {
			layoutType = 'md';

		} else if (viewportWidth > breakPoint1) {
			layoutType = 'sm';

		} else {
			layoutType = 'xs';
		}

		if (currentLayoutType != layoutType) {
			$(document).trigger( "layoutTypeChange" );
		}
	};


	/*
	 * updateLayoutOrientation
	 * Sets layout orientation tag
	 */
	var updateLayoutOrientation = function() {
		//window.console && console.log('layout.updateLayoutOrientation()');

    	if( viewportHeight < viewportWidth ){
    		if( layoutOrientation != 'landscape' ){
    			layoutOrientation = 'landscape';
    			$(document).trigger( "layoutOrientationChange" );
    		}
    	} else {
    		if( layoutOrientation != 'portrait' ){
    			layoutOrientation = 'portrait';
    			$(document).trigger( "layoutOrientationChange" );
    		}
    	}
	};


	/*
	 * initScroll
	 */
	var initScroll = function() {
		window.console && console.log('mainUI.initScroll()');
	
		$(window).unbind('DOMMouseScroll mousewheel scroll');
		$(window).on('DOMMouseScroll mousewheel scroll', scrollHandler );
	};


	/*
	 * scrollHandler
	 */
	var scrollHandler = function(){
		window.console && console.log('layout.scrollHandler()');
		
		// Loop through the loaded modules and call each module resize method
		for( var i = 0; i < loadedModules.length; i++ ) {
			if(loadedModules[i].name != 'layout' && typeof loadedModules[i].scroll == 'function'){
				loadedModules[i].scroll();
			}
		}
	};


	/*
	 * initResize
	 * Create viewport resizing events
	 */
	var initResize = function(){
		//window.console && console.log('layout.initResize()');

		// Initial resize event on page load
		resizeHandler();

		// Window resize event
		$(window).resize( function() {
			clearTimeout(resizeTimer);
	  		resizeTimer = setTimeout(resizeHandler, resizeDelay);
		});

		// Mobile orientation change event
		var supportsOrientationChange = "onorientationchange" in window,
		    orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

		if ( window.addEventListener ) {
			window.addEventListener(orientationEvent, function() {
				var orientation = window.orientation;
				clearTimeout(orientationTimer);
		        orientationTimer = setTimeout(function () {
					clearTimeout(resizeTimer);
		  			resizeTimer = setTimeout(resizeHandler, resizeDelay);
		    	}, orientationDelay);
		        
			}, false);
		}
	};


	/*
	 * resizeHandler
	 */
	var resizeHandler = function(){
		window.console && console.log('layout.resizeHandler()');
		
		// Get list of loaded js modules for resize event
		loadedModules = app.getLoadedModules();

		updateDimensions();
		updateLayoutType();
		updateLayoutOrientation();
		setLayoutClasses();
		resizeElements();
	};


	/*
	 * setLayoutClasses
	 * Appends layout and orientation tags as classes to <body>
	 */
	var setLayoutClasses = function(){
		window.console && console.log('layout.setLayoutClasses()');

		// Remove all classes from body
		$('body').removeClass('xl lg md sm xs landscape portrait');

		// Set layout and orientation classes
		$('body').addClass(layoutType);
		$('body').addClass(layoutOrientation);
	};


	/*
	 * resizeElements
	 * on page resize checks to see which modules are
	 * loaded so only elements of current page are resized
	 */
	var resizeElements = function(){
		window.console && console.log('layout.resizeElements()');

		// Loop through the loaded modules and call each module resize method
		for( var i = 0; i < loadedModules.length; i++ ) {
			if(loadedModules[i].name != 'layout' && typeof loadedModules[i].resize == 'function'){
				loadedModules[i].resize();
			}
		}
	};




 	// Public
 	// ---------------------------------------------------------
 	// ---------------------------------------------------------

	return {
		// Module name id for dynamic initialization
		name: 'layout',

		/*
		 * init
		 * Main init function called when
		 * module target content is loaded
		 */
	    init: function(){
	    	window.console && console.log('layout.init()');

			initModule();
	    },

		/*
		 * revealPage
		 * Body tag has default opacity of 0.
		 * Body fades to opacity 1 after fonts have loaded to avoid font flicker.
		 */
		revealPage: function(){
			window.console && console.log('layout.revealPage()');

			TweenMax.to( $('body'), 0.5, { opacity: 1, ease: Power2.easeOut });
			pageRevealed = true;
		},

		/*
		 * isPageRevealed
		 */
		isPageRevealed: function(){
			window.console && console.log('layout.isPageRevealed()');
			return pageRevealed;
		},

	    /*
		 * getLayoutType
		 */
	    getLayoutType: function(){
	    	//window.console && console.log('layout.getLayoutType()');
	    	return layoutType;
	    },

	    /*
		 * getLayoutOrientation
		 */
	    getLayoutOrientation: function(){
	    	//window.console && console.log('layout.getLayoutOrientation()');
	    	return layoutOrientation;
	    },

	    /*
		 * getViewportWidth
		 */
	    getViewportWidth: function(){
	    	return viewportWidth;
	    },

	    /*
		 * getViewportHeight
		 */
	    getViewportHeight: function(){
	    	return viewportHeight;
	    },

	    /*
		 * resize
		 * Called by layout module on viewport resize event
		 */
	    resize: function(){
	    	//window.console && console.log('layout.resize()');
	    	resizeHandler();
	    }
	};
})();
