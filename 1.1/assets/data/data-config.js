
/*
 * JS App Config
 */

var config = {

	// Default template on site load
	templateDefault: 'home',

    // Template modules
	templates: {

		'home': {
			file: 'templates/home/home.php',
			route: '/home',
			modules: [ hoverBox ]
		},

		// UX
		'ux': {
			file: 'templates/ux/ux.php',
			route: '/ux',
			modules: [ hoverBox ]
		},

		// Design
		'design': {
			file: 'templates/design/design.php',
			route: '/design',
			modules: [ hoverBox ]
		},

		// Code
		'code': {
			file: 'templates/code/code.php',
			route: '/code',
			modules: [ hoverBox ]
		},

		'genesis-redesign': {
			file: 'templates/code/genesis-redesign/genesis-redesign.php',
			route: '/code/genesis-redesign',
			modules: [ hoverBox ]
		},

		'samsung-experience': {
			file: 'templates/code/samsung-experience/samsung-experience.php',
			route: '/code/samsung-experience',
			modules: [ hoverBox ]
		},

		'mophie-outride': {
			file: 'templates/code/mophie-outride/mophie-outride.php',
			route: '/code/mophie-outride',
			modules: [ hoverBox ]
		},

		'find-your-7': {
			file: 'templates/code/find-your-7/find-your-7.php',
			route: '/code/find-your-7',
			modules: [ hoverBox ]
		},

		'turnip': {
			file: 'templates/code/turnip/turnip.php',
			route: '/code/turnip',
			modules: [ hoverBox ]
		},

		'sim-city-edu': {
			file: 'templates/code/sim-city-edu/sim-city-edu.php',
			route: '/code/sim-city-edu',
			modules: [ hoverBox ]
		},

		'installations': {
			file: 'templates/code/installations/installations.php',
			route: '/code/installations',
			modules: [ hoverBox ]
		},

		'hyundai-redesign': {
			file: 'templates/code/hyundai-redesign/hyundai-redesign.php',
			route: '/code/hyundai-redesign',
			modules: [ hoverBox ]
		},

		'products-and-services': {
			file: 'templates/code/products-and-services/products-and-services.php',
			route: '/code/products-and-services',
			modules: [ hoverBox ]
		},



		// Me
		'me': {
			file: 'templates/me/me.php',
			route: '/me',
			modules: []
		}
		
	},
    
	// Modules that core js app depends on, should not be removed.
	coreModules: [ layout, fonts, templateLoader, hash ],

    // Default modules loaded for all templates.
    defaultModules: [ waypoints, toolbox, blocks, mainUI, squareBox ]

    // Additional config options
};




