<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'turnip.css'; ?>
</style>



<!-- .template.code -->
<div class="template turnip">
	
	<div class="row base waypoint">
		<!-- .block -->
		<div class="block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title page-title">turnip.</div>
		</div>
		<!-- .block -->
	</div>


	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">Everyone hates doing timesheets. Every timesheet application I have ever used has been horribly overbloated, slow, difficult to use and basically a big pain in the ass. I've always tracked my time with a text document, the same text document I use as my todo list.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">So, I decided to build a simpler timesheet todo list application, that boiled everything down to the basics. Write down a list of things you have to do, and be able to quickly tick time intervals on those todo items if you wanted to. And thus Turnip became a thing.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<div class="hover-box" data-image-backup-url="assets/images/turnip/turnip-hero.jpg"></div>
			</div>
		</div>
		<!-- .block -->

	</div>



	<!-- 2-up -->
	<div class="promo-blocks flex-row row base extra-space">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/turnip/turnip-1.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/turnip/turnip-2.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<div class="hover-box" data-image-backup-url="assets/images/turnip/turnip-5.jpg"></div>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/turnip/turnip-4.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/turnip/turnip-3.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>



	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/turnip/turnip-7.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/turnip/turnip-6.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/turnip/turnip-8.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/turnip/turnip-9.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>



</div>
<!-- .template.code -->