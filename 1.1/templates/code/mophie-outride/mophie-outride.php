<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'mophie-outride.css'; ?>
</style>


<!-- .template.code -->
<div class="template mophie-outride">
	
	<div class="row base waypoint">
		<!-- .block -->
		<div class="block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title page-title">mophie outride.</div>
		</div>
		<!-- .block -->
	</div>


	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">The Mophie Outride was a competitor product to the GoPro. This site was not so much a product site, but a media archive and social sharing application for Outride content. </div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">User's could take photos and videos with thier devices, and upload that content to this site. Users could add friends, comment on content, create playlists, etc. It is very YouTubesque in many regards, but directly linked to the products.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/mophie-outride/Outride_Web_ScreenH_060612.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/mophie-outride/Outride_Web_ScreenD1_060612.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 3-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/mophie-outride/Outride_Web_Screen3a_060612.jpg"/>
			</div>
		</div>
		<!-- .block -->

				<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/mophie-outride/Outride_Web_Screen4a2_060612.jpg"/>
			</div>
		</div>
		<!-- .block -->

				<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/mophie-outride/Outride_Web_ScreenC_061412.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/mophie-outride/Outride_Web_ScreenU1_060812.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/mophie-outride/Outride_Web_ScreenU3_060812.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 3-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/mophie-outride/Outride_Web_ScreenR_060812.jpg"/>
			</div>
		</div>
		<!-- .block -->

				<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/mophie-outride/Outride_Web_ScreenC1_061412.jpg"/>
			</div>
		</div>
		<!-- .block -->

				<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/mophie-outride/Outride_Web_ScreenL_060812.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

</div>
<!-- .template.code -->