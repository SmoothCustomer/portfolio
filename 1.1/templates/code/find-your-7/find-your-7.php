<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'find-your-7.css'; ?>
</style>



<!-- .template.code -->
<div class="template find-your-7">
	
	<div class="row base waypoint">
		<!-- .block -->
		<div class="block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title page-title">find your 7.</div>
		</div>
		<!-- .block -->
	</div>


	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">Find Your 7 was a promotion for the new Hyundai Santa Fe, and was featured in a commercial that would drive traffic to the site during the superbowl. This project, despite it's simple appearance had a lot of technical parts to it. Users would sign into the experience using Facebook, and once they authorized the app, it would pick 7 of their friends to be part of their crew. </div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">This crew had different personalities profiles for each person, and once they were selected a dynamic video was generated using Facebook content that could then be shared or whatever. The whole crew was then entered into a sweeps for a chance to win tickets to the next years superbowl.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<div class="hover-box" data-image-backup-url="assets/images/find-your-7/fy7-hero.jpg"></div>
			</div>
			<div class="promo-block-description body">This was actually a fun exercise in mining though social data and developing an algorith for selecting friends of the user the fit the profile for each of the personalities of the crew. I ran several tests, seeing how much data I could get from the apps set of permissions. This was back in the early days of Facebook apps, so the privacy restrictions were much looser, so it's a little scary how much I was able to gather about the user and all their friends.</div>
		</div>
		<!-- .block -->

	</div>



	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/fy7-screen-1.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/fy7-screen-2.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/fy7-screen-3.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/fy7-screen-4.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint unconnected">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">To give an example, one of the personalities of the crew memebers was The Brain. So to select which of the user's friends should take that role I searched through all their friends, and tallied who had the most schooling listed, who had the most books liked, etc.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">For other roles I seached through conversations for tags I defined. I figured out who the user's closest friends were by who they exchanged messages with the most, who was tagged in the most photos, etc.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/fy7-screen-5.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/fy7-screen-6.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/fy7-screen-7.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/fy7-screen-8.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<div class="hover-box" data-video-url="assets/video/cinema-find-your-7.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 3-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/movie001.jpg"/>
			</div>
		</div>
		<!-- .block -->

				<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/movie003.jpg"/>
			</div>
		</div>
		<!-- .block -->

				<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/movie004.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 3-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/movie005.jpg"/>
			</div>
		</div>
		<!-- .block -->

				<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/movie006.jpg"/>
			</div>
		</div>
		<!-- .block -->

				<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/find-your-7/movie007.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>




</div>
<!-- .template.code -->