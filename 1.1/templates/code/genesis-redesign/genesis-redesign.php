<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'genesis-redesign.css'; ?>
</style>


<!-- .template.code -->
<div class="template code">
	
	<div class="row base waypoint">
		<!-- .block -->
		<div class="block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title page-title">genesis redesign prototyping.</div>
		</div>
		<!-- .block -->
	</div>


	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">Genesis is a new luxury automotive brand. The idea here was to take a look at the luxury shopping experience and run an experiment on how the traditional auto site could be reimagined to be more, well luxury. Could it be a minimal, product focused, and guided experience. By guided I mean a shopping process that feels almost like you have a concierge helping you along the way. Linear and step based.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">What I did was try and reduce elements in the viewport down to a single visual focal point, a product photo, video, map etc. And at all times, have no more than 2 primary actions available. Also I wanted an guiding element, a tooltip, or clear marker for what the next step in the process would be. Alternatively, an non-intrusive, but readily available chat ai, that could not just respond to questions, but offer suggestions, and even navigate the page for the user when proper interactions occured.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<div class="hover-box" data-video-url="assets/video/cinema-genesis-overview.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
			</div>
			<div class="promo-block-description body">Notice that the site is panel based. Each step, or bit of information in contained within the viewport at all times. The scrollbar menu on the side reveals anchor links for each panel when user interacts with it. Chat feature does not lay over content, but works with it instead, allowing chat and browsing to occur simultaneously.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up square-boxes -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<div class="promo-block-title title">materials explorer.</div>
				<div class="hover-box" data-hover-scale="1.05" data-video-url="assets/video/square-genesis-materials.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
			</div>
			<div class="promo-block-description body">Interesting interface for exploring materials. Whole thing is basically 1 big SVG that has multiple layers and masks that animate to reveal the material images and swatches.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<div class="promo-block-title title">360 views.</div>
				<div class="hover-box" data-hover-scale="1.05" data-video-url="assets/video/square-genesis-360.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
			</div>
			<div class="promo-block-description body">Full-screen interior and exterior 360 views that user can drag to rotate.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<div class="promo-block-title title"></div>
				<div class="hover-box" data-video-url="assets/video/cinema-genesis-chat.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
			</div>
			<div class="promo-block-description body">Here you can see how the chat ai fully integrates with the site content. It again does not lay over the content but scales the content so that both can be fully visible and functional. The chat knows what section the user is looking at and con offer up, unprompted, content and suggestions that enhance the section they are in. User can interact directly with the ai and have thier input cause the site to navigate to another section, or even take actions they would otherwise use traditional ui for.</div>
		</div>
		<!-- .block -->

	</div>



	<!-- 2-up square-box-cinema -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/genesis-redesign/genesis-redesign-screen-caps-1.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="ux" href="/ux" alt="ux">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/genesis-redesign/genesis-redesign-screen-caps-2.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up square-box-cinema -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/genesis-redesign/genesis-redesign-screen-caps-4.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="ux" href="/ux" alt="ux">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/genesis-redesign/genesis-redesign-screen-caps-5.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up square-box-cinema -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/genesis-redesign/genesis-redesign-screen-caps-7.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="ux" href="/ux" alt="ux">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/genesis-redesign/genesis-redesign-screen-caps-9.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up square-box-cinema -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/genesis-redesign/genesis-redesign-screen-caps-3.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="ux" href="/ux" alt="ux">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/genesis-redesign/genesis-redesign-screen-caps-10.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->

	</div>


</div>
<!-- .template.code -->