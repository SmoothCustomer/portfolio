<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'products-and-services.css'; ?>
</style>


<!-- .template.code -->
<div class="template products-and-services">
	
	<div class="row base waypoint">
		<!-- .block -->
		<div class="block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title page-title">products &amp; services.</div>
		</div>
		<!-- .block -->
	</div>


	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">Developing product and serivce concepts that solve business problems and creating protoypes to vet the ideas as well as sell them in to clients has been a big part of my day to day over the past few years. Hyundai drive was developed to be an on demand, location based digital services platform to augment the ownership of Hyundai vehicles. That pilot program became the backbone of the very successful Genesis Sales Valet program. </div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">It was also the starting point for a larger suite of luxury services still in development Genesis One. Campfire is a social product concept around creating location based groups for business, events, or any gethering that helps people identify and communicate with new people aournd them.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 1-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/drive-hero.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 1-up -->
	<div class="promo-blocks flex-row row base extra-space">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/drive-roadmap.png"/>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 1-up -->
	<div class="promo-blocks flex-row row base extra-space">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/drive-screens.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>






	<!-- 1-up -->
	<div class="promo-blocks flex-row row base extra-space">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/sales-valet-hero.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/sales-valet-1.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/sales-valet-2.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>







	<!-- 1-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/genesis-one2.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 3-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/Genesis-App-v2-r1-concierge-button.png"/>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/Genesis-App-v2-r1-conceirge-speaking.png"/>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/Genesis-App-v2-r1-cards-scrolled.png"/>
			</div>
		</div>
		<!-- .block -->
		
	</div>

	<!-- 3-up -->
	<div class="promo-blocks flex-row row base extra-space">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/Genesis-App-Cards1.png"/>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/Genesis-App-Cards2.png"/>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/Genesis-App-Cards3.png"/>
			</div>
		</div>
		<!-- .block -->
		
	</div>

	<!-- 3-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/Genesis-App-v2-r1-top-requests.png"/>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/Genesis-App-v2-r1-config1.png"/>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/Genesis-App-v2-r1-config2.png"/>
			</div>
		</div>
		<!-- .block -->
		
	</div>




	<!-- 4-up -->
	<div class="promo-blocks flex-row row base extra-space">

		<!-- .block -->
		<div class="promo-block block col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/campfire-r2-2.png"/>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/campfire-r1-compass.png"/>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/campfire-r1-list.png"/>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/products-and-services/campfire-r1-person.png"/>
			</div>
		</div>
		<!-- .block -->


	</div>

</div>
<!-- .template.c