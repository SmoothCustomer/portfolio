<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'samsung-experience.css'; ?>
</style>


<!-- .template.code -->
<div class="template code">
	
	<div class="row base waypoint">
		<!-- .block -->
		<div class="block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title page-title">samsung experience demos.</div>
		</div>
		<!-- .block -->
	</div>


	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">These demos ran on all the various models from the Samsung ATIV line. Build as Windows edge applicaitons, they were baked into special versions of Windows so that they could overide the default screensaver mechanisms. Demos would run attractor animations until users interacted with the machine. Then the demo would kick out of attractor mode and into a full-screen demo experience.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">At any time the user could exit out of the demo to the Windows desktop and use the system normally. But once the machine went idle, the demo applciation would automatically relaunch into the attractor mode, restarting the whole experience.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-store-display.jpg"></div>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint extra-space">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<div class="promo-block-title title"></div>
				<div class="hover-box" data-video-url="assets/video/cinema-samsung-experience-overview.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up square-box-cinema -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-screens-1.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="ux" href="/ux" alt="ux">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-screens-2.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up square-box-cinema -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-screens-3.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="ux" href="/ux" alt="ux">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-screens-4.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up square-box-cinema -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-screens-5.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="ux" href="/ux" alt="ux">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-screens-6.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up square-box-cinema -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-screens-7.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="ux" href="/ux" alt="ux">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-screens-8.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up square-box-cinema -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-screens-9.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<a class="route" data-id="ux" href="/ux" alt="ux">
					<div class="hover-box" data-hover-scale="1.05" data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-screens-10.jpg"></div>
				</a>
			</div>
		</div>
		<!-- .block -->

	</div>



</div>
<!-- .template.code -->