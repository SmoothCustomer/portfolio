<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'code.css'; ?>
</style>


<!-- .template.code -->
<div class="template code">
	

	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="promo-block-title title">genesis redesign.</div>
					<div class="hover-box" data-hover-scale="1.05" data-image-backup-url="assets/images/code-project-preview-images-genesis-redesign.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Exploratory prototyping and full redesign of genesis.com, pushing the boundaries of minimal and focused design, as well as ai guided navigation.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="hyundai-redesign" href="/code/hyundai-redesign" alt="hyundai-redesign">
					<div class="promo-block-title title">hyundai redesign.</div>
					<div class="hover-box" data-hover-scale="1.05" data-image-backup-url="assets/images/code-project-preview-images-hyundai-redesign.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Re-architected, and redesigned the entire Hyundai digital ecosystem, including main web property hyundai.com. Full site prototype of new structure, as well as new features and interfaces.</div>
		</div>
		<!-- .block -->

	</div>


	<div class="flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="samsung-experience" href="/code/samsung-experience" alt="samsung experience">
					<div class="promo-block-title title">samsung experience.</div>
					<div class="hover-box" data-hover-scale="1.05"  data-video-url="" data-image-backup-url="assets/images/samsung-experience/samsung-experience-store-display.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">In store self running demos for the ATIV product line, featured at the Samsung Experience.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="find-your-7" href="/code/find-your-7" alt="find-your-7">		
					<div class="promo-block-title title">find your 7.</div>
					<div class="hover-box" data-hover-scale="1.05"  data-video-url="" data-image-backup-url="assets/images/code-project-preview-images-find-your-7.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Superbowl promotional site that integrated social data, custom video generation based on data. Promoted during Superbowl, so set up to be scalable to high demand traffic spike.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="mophie-outride" href="/code/mophie-outride" alt="mophie-outride">		
					<div class="promo-block-title title">mophie outride.</div>
					<div class="hover-box" data-hover-scale="1.05"  data-video-url="" data-image-backup-url="assets/images/code-project-preview-images-mophie.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Front-end development for Mophie's GoPro competitor product website.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">	
				<a class="route" data-id="turnip" href="/code/turnip" alt="turnip">	
					<div class="promo-block-title title">turnip.</div>
					<div class="hover-box" data-hover-scale="1.05"  data-video-url="" data-image-backup-url="assets/images/code-project-preview-images-turnip.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Time tracking and todo list web application.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="installations" href="/code/installations" alt="installations">
					<div class="promo-block-title title">installations.</div>
					<div class="hover-box" data-hover-scale="1.05"  data-video-url="" data-image-backup-url="assets/images/code-project-preview-images-installations.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Various touch display installations, designed to have 24/7 up time and remote application management.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="sim-city-edu" href="/code/sim-city-edu" alt="sim-city-edu">		
					<div class="promo-block-title title">sim city edu.</div>
					<div class="hover-box" data-hover-scale="1.05"  data-video-url="" data-image-backup-url="assets/images/code-project-preview-images-simcity.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Lesson plan authoring and sharing tool for teachers using SimCity as a learning platform.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="products-and-services" href="/code/products-and-services" alt="products-and-services">		
					<div class="promo-block-title title">products &amp; services.</div>
					<div class="hover-box" data-hover-scale="1.05"  data-video-url="" data-image-backup-url="assets/images/code-project-preview-images-products.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Prototyping, UX design and product development for several products and services.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="video-configurator" href="/code/video-configurator" alt="video-configurator">		
					<div class="promo-block-title title">video configurator.</div>
					<div class="hover-box" data-hover-scale="1.05"  data-video-url="" data-image-backup-url="assets/images/code-project-preview-images-video-config.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Video based automotive configurator. Allows consumers to configure vehicles in tandem with running footage and see updates in real time.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="code" href="/code" alt="code">		
					<div class="promo-block-title title">conversational ai experiments.</div>
					<div class="hover-box" data-hover-scale="1.05"  data-video-url="" data-image-backup-url="assets/images/code-project-preview-images-ai.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Experiments in conversational ai.</div>
		</div>
		<!-- .block -->

	</div>

</div>
<!-- .template.code -->