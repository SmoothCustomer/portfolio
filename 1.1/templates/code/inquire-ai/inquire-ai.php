<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'code.css'; ?>
</style>


<!-- .template.code -->
<div class="template code">
	
	<div class="row base waypoint">
		<!-- .block -->
		<div class="block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title page-title">genesis redesign.</div>
		</div>
		<!-- .block -->
	</div>

	<div class="promo-blocks row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-half" data-inner-padding="10px">
				<a class="route" data-id="design" href="/design" alt="design">
					<div class="promo-block-title title">design.</div>
					<div class="hover-box" data-video-url="assets/video/fpo-video-rollover-a-1420x710.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Maecenas commodo sit amet ex ac pharetra. Morbi ac eleifend lorem. Morbi et diam ac lorem ultrices fermentum. Phasellus feugiat sapien sit amet dui tristique vehicula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sed felis rutrum nunc pellentesque suscipit. Curabitur pellentesque eleifend sem, et venenatis lorem maximus ac nullam commodo ex turpis.</div>
		</div>
		<!-- .block -->

	</div>

</div>
<!-- .template.code -->