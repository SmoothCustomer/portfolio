<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'installations.css'; ?>
</style>



<!-- .template.code -->
<div class="template installations">
	
	<div class="row base waypoint">
		<!-- .block -->
		<div class="block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title page-title">installations.</div>
		</div>
		<!-- .block -->
	</div>


	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">I really like building real world installations. They are a fun challenge in that they have to be be 100% bullet proof. These things don't get rebooted, restarted. They run for 24 hours a day, 7 days a week. They are often at some remote location you can't just pop over to in case something goes wrong, so you need to method of remote control.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body"> And they have to be able to stand up to the punishment and relentless destructive power of children. Your event handling has to be spot on, no memory leaks. Just solid coding.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-widescreen" data-inner-padding="15px">
				<div class="hover-box" data-video-url="assets/video/widescreen-cups.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/installations/cups.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/installations/clash.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint extra-space">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-cinema" data-inner-padding="15px">
				<div class="hover-box" data-video-url="" data-image-backup-url="assets/images/installations/installations-hero.jpg"></div>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/installations/adlScreenSnapz011.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/installations/adlScreenSnapz012.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>



	<!-- Full width hero square-box-cinema -->
	<div class="promo-blocks row base waypoint extra-space">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/installations/miocean-1.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/installations/miocean-2.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/installations/miocean-3.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>


</div>
<!-- .template.code -->