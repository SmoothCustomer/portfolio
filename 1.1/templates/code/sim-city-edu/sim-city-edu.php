<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'sim-city-edu.css'; ?>
</style>


<!-- .template.code -->
<div class="template sim-city-edu">
	
	<div class="row base waypoint">
		<!-- .block -->
		<div class="block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title page-title">sim city edu.</div>
		</div>
		<!-- .block -->
	</div>


	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">This ended up being one of the more instersting Wordpress builds I've ever done. The site is a lesson plan authoring tool, where teachers could create and account, design and publish lesson plans, share them with other teachers, and a whole bunch of other stuff. Why it was interesting was that the authoring mechanism was so complex, and so dense, it almost became an inside-out version of Wordpress.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">Users could create accounts, publish and preview documents with many of the same tools you would have available with the admin screens of Wordpress. Oh ya, and the lesson plans were for using SimCity as a teching tool.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/sim-city-edu/simcity-5.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/sim-city-edu/simcity-4.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/sim-city-edu/simcity-3.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/sim-city-edu/simcity-6.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/sim-city-edu/simcity-7.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/sim-city-edu/simcity-8.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>


	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/sim-city-edu/simcity-9.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/sim-city-edu/simcity-10.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>



</div>
<!-- .template.code -->