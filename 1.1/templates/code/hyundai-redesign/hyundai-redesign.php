<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'hyundai-redesign.css'; ?>
</style>


<!-- .template.code -->
<div class="template hyundai-redesign">
	
	<div class="row base waypoint">
		<!-- .block -->
		<div class="block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title page-title">hyundai redesign.</div>
		</div>
		<!-- .block -->
	</div>


	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">The main Hyundai digital property hyundai.com had not had a redesign or reachitechting in over a decade. There were many new technologies and tools available, and many old legacy systems still in place. </div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">This exercise was not only was to modernize the design of the site, but reorganize it to be more user centric in it's offerings. Also, to fully leverage the new suite of available tools and technologies, better integrate all the other digital properties, and introduce concepts like personalization and dynamic content.</div>
		</div>
		<!-- .block -->

	</div>


	<!-- Full width hero square-box-widescreen -->
	<div class="promo-blocks row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-widescreen" data-inner-padding="15px">
				<div class="hover-box" data-video-url="assets/video/widescreen-hcom-overview.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
			</div>
			<div class="promo-block-description body">One of the first major insights in redesigning this site, was that almost every action a user took on the site first required them to select a vehicle. Users would have to continually reselect their preferred vehicle over and over again for all the major site features and content pages. So the new design flipped that dynamic by organizing the site around the vehicles, essentially reducing the site page count down to roughly a dozen pages, eahc page representing a vehicle. All the relevant actions and tools would the be in context of the current vehicle the user was viewing, no longer requiring them to continually jump around from tool to tool to get what they need.</div>
		</div>
		<!-- .block -->

	</div>



	<!-- 2-up widescreen-boxes -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-widescreen" data-inner-padding="15px">
				<div class="promo-block-title title">materials explorer.</div>
				<div class="hover-box" data-video-url="assets/video/widescreen-hcom-sizzle-1.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
			</div>
			<div class="promo-block-description body">Maecenas commodo sit amet ex ac pharetra. Morbi ac eleifend lorem. Morbi et diam ac lorem ultrices fermentum. Phasellus feugiat sapien sit amet dui tristique vehicula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sed felis rutrum nunc pellentesque suscipit. Curabitur pellentesque eleifend sem, et venenatis lorem maximus ac. Nullam commodo ex turpis, sit amet rhoncus est pellentesque quis.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper square-box square-box-widescreen" data-inner-padding="15px">
				<div class="promo-block-title title">360 views.</div>
				<div class="hover-box" data-video-url="assets/video/widescreen-hcom-sizzle-2.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
			</div>
			<div class="promo-block-description body">Maecenas commodo sit amet ex ac pharetra. Morbi ac eleifend lorem. Morbi et diam ac lorem ultrices fermentum. Phasellus feugiat sapien sit amet dui tristique vehicula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sed felis rutrum nunc pellentesque suscipit. Curabitur pellentesque eleifend sem, et venenatis lorem maximus ac nullam commodo ex turpis.</div>
		</div>
		<!-- .block -->

	</div>



	<!-- 2-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/hyundai-redesign/hcom-1.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/hyundai-redesign/hcom-2.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 2-column text -->
	<div class="promo-blocks flex-row row base waypoint">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">Vehicle configuration has traditionally been a stepped based process. This build tool has taken several cues from the video game world, and how avatars/characters are often configured. Here the vehicle remains the focal point, and the user asyncronously selects features to modify, while being able to immediately see the result. The whole page serves as your summary and can be bookmarked and shared at any point without having to jump back and forth between steps.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-description body">Here I introduced the concept of a content liking system. At any point a user encounters content: photos, videos, specs, colors that they have interest in, they can click the like icon and that content gets saved to their content drawer. At any point the user can open up this drawer and generate a dynamic pdf or email based on their liked content.</div>
		</div>
		<!-- .block -->

	</div>

	<!-- 3-up -->
	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/hyundai-redesign/hcom-3.jpg"/>
			</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/hyundai-redesign/hcom-4.jpg"/>
			</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12 waypoint">
			<div class="promo-block-inner-wrapper">
				<img src="assets/images/hyundai-redesign/hcom-5.jpg"/>
			</div>
		</div>
		<!-- .block -->

	</div>






</div>
<!-- .template.code -->