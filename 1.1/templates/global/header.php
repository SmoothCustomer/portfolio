<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
    <?php
        if (isset($_SERVER['HTTP_USER_AGENT']) &&
        (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
            header('X-UA-Compatible: IE=edge,chrome=1');
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>Mike Stone - UX, Design, Code</title>

    <!-- Bootstrap v3.3.7 Grid -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Style Sheets -->
    <link href="assets/css/main.css" rel="stylesheet">
	<link href="assets/css/bootstrap-flexi.css" rel="stylesheet">

	<!-- JS App Style Sheets -->
    <link href="assets/css/modules/module-blocks.css" rel="stylesheet">
	<link href="assets/css/modules/module-hover-box.css" rel="stylesheet">
	<link href="assets/css/modules/module-square-box.css" rel="stylesheet">

</head>