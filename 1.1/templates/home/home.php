<?php
/*
Template Name: Home
*/
?>

<style>
<?php include 'home.css'; ?>
</style>


<!-- .template.home -->
<div class="template home">


	<div class="promo-blocks flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="10px">
				<a class="route" data-id="genesis-redesign" href="/code/genesis-redesign" alt="genesis redesign">
					<div class="promo-block-title title">genesis redesign.</div>
					<div class="hover-box" data-video-url="assets/video/fpo-video-rollover-a-700x700.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Maecenas commodo sit amet ex ac pharetra. Morbi ac eleifend lorem. Morbi et diam ac lorem ultrices fermentum. Phasellus feugiat sapien sit amet dui tristique vehicula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sed felis rutrum nunc pellentesque suscipit. Curabitur pellentesque eleifend sem, et venenatis lorem maximus ac. Nullam commodo ex turpis, sit amet rhoncus est pellentesque quis.</div>
		</div>
		<!-- .block -->
		
		<!-- .block -->
		<div class="promo-block block col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="10px">
				<a class="route" data-id="ux" href="/ux" alt="ux">
					<div class="promo-block-title title">ux.</div>
					<div class="hover-box" data-video-url="assets/video/fpo-video-rollover-b-700x700.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Maecenas commodo sit amet ex ac pharetra. Morbi ac eleifend lorem. Morbi et diam ac lorem ultrices fermentum. Phasellus feugiat sapien sit amet dui tristique vehicula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sed felis rutrum nunc pellentesque suscipit. Curabitur pellentesque eleifend sem, et venenatis lorem maximus ac nullam commodo ex turpis.</div>
		</div>
		<!-- .block -->

	</div>

	<div class="promo-blocks row base">

		<!-- .block -->
		<div class="promo-block block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="promo-block-inner-wrapper square-box square-box-half" data-inner-padding="10px">
				<a class="route" data-id="design" href="/design" alt="design">
					<div class="promo-block-title title">design.</div>
					<div class="hover-box" data-video-url="assets/video/fpo-video-rollover-a-1420x710.mp4" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Maecenas commodo sit amet ex ac pharetra. Morbi ac eleifend lorem. Morbi et diam ac lorem ultrices fermentum. Phasellus feugiat sapien sit amet dui tristique vehicula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sed felis rutrum nunc pellentesque suscipit. Curabitur pellentesque eleifend sem, et venenatis lorem maximus ac nullam commodo ex turpis.</div>
		</div>
		<!-- .block -->

	</div>



	<div class="row base">

		<!-- .block -->
		<div class="new block col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="title">new things.</div>
		</div>
		<!-- .block -->

	</div>


	<div class="flex-row row base">

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="code" href="/code" alt="code">
					<div class="promo-block-title title">hi there.</div>
					<div class="hover-box" data-video-url="" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce urna dui, tincidunt et tincidunt eu, tincidunt sit amet nulla. Fusce dapibus lectus turpis, ac vestibulum leo elementum non. Donec sit amet euismod lectus.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="code" href="/code" alt="code">		
					<div class="promo-block-title title">hi there.</div>
					<div class="hover-box" data-video-url="" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">In fringilla purus sed odio elementum aliquam. Maecenas venenatis, leo vitae euismod porta, ex libero tincidunt ipsum, vel vehicula sem tellus vitae nisi. Morbi sit amet malesuada ante.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="code" href="/code" alt="code">		
					<div class="promo-block-title title">hi there.</div>
					<div class="hover-box" data-video-url="" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Aliquam ultricies, lorem vitae varius sagittis, odio odio dapibus tellus, a scelerisque est nunc eu dolor. Nunc a turpis vulputate, mattis erat et, sodales metus. Praesent et metus ut risus accumsan tincidunt eget vitae arcu.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">	
				<a class="route" data-id="code" href="/code" alt="code">	
					<div class="promo-block-title title">hi there.</div>
					<div class="hover-box" data-video-url="" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Maecenas commodo sit amet ex ac pharetra. Morbi ac eleifend lorem. Morbi et diam ac lorem ultrices fermentum. Phasellus feugiat sapien sit amet dui tristique vehicula.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="code" href="/code" alt="code">
					<div class="promo-block-title title">hi there.</div>
					<div class="hover-box" data-video-url="" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce urna dui, tincidunt et tincidunt eu, tincidunt sit amet nulla. Fusce dapibus lectus turpis, ac vestibulum leo elementum non. Donec sit amet euismod lectus.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="code" href="/code" alt="code">		
					<div class="promo-block-title title">hi there.</div>
					<div class="hover-box" data-video-url="" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">In fringilla purus sed odio elementum aliquam. Maecenas venenatis, leo vitae euismod porta, ex libero tincidunt ipsum, vel vehicula sem tellus vitae nisi. Morbi sit amet malesuada ante.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="code" href="/code" alt="code">		
					<div class="promo-block-title title">hi there.</div>
					<div class="hover-box" data-video-url="" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Aliquam ultricies, lorem vitae varius sagittis, odio odio dapibus tellus, a scelerisque est nunc eu dolor. Nunc a turpis vulputate, mattis erat et, sodales metus. Praesent et metus ut risus accumsan tincidunt eget vitae arcu.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="code" href="/code" alt="code">		
					<div class="promo-block-title title">hi there.</div>
					<div class="hover-box" data-video-url="" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">Maecenas commodo sit amet ex ac pharetra. Morbi ac eleifend lorem. Morbi et diam ac lorem ultrices fermentum. Phasellus feugiat sapien sit amet dui tristique vehicula.</div>
		</div>
		<!-- .block -->

		<!-- .block -->
		<div class="promo-block block col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="promo-block-inner-wrapper square-box" data-inner-padding="15px">
				<a class="route" data-id="code" href="/code" alt="code">		
					<div class="promo-block-title title">hi there.</div>
					<div class="hover-box" data-video-url="" data-image-backup-url="assets/images/fpo-promo-1.jpg"></div>
				</a>
			</div>
			<div class="promo-block-description body">In fringilla purus sed odio elementum aliquam. Maecenas venenatis, leo vitae euismod porta, ex libero tincidunt ipsum, vel vehicula sem tellus vitae nisi. Morbi sit amet malesuada ante.</div>
		</div>
		<!-- .block -->

	</div>


</div>
<!-- .template.home -->