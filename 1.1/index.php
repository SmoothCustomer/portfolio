<?php
    // Mobile Detection
    require_once 'assets/php/Mobile-Detect-2.8.26/Mobile_Detect.php';
    $detect = new Mobile_Detect;

	$mobile = ( $detect->isMobile() ? 'true' : 'false' );
	$tablet = ( $detect->isTablet() ? 'true' : 'false' );
	$ios = ( $detect->isiOS() ? 'true' : 'false' );
	$android = ( $detect->isAndroidOS() ? 'true' : 'false' );

	echo '<script>' . PHP_EOL;
	echo 'var client = { ' . PHP_EOL;
		echo '	mobile: ' . $mobile . ',' . PHP_EOL;
		echo '	tablet: ' . $tablet . ',' . PHP_EOL;
		echo '	ios: ' . $ios . ',' . PHP_EOL;
		echo '	android: ' . $android . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '</script>';
?>
<?php include 'templates/global/header.php'; ?>

<body>
    
    <!-- #viewport -->
    <div id="viewport">

    	<!-- #masthead -->
    	<div id="masthead">

    		<div id="main-ui">
    			<div id="logo"><a class="route" data-id="home" href="/home" alt="home"><img src="assets/images/logo.svg" alt="logo"></a></div>
    			<div id="main-menu">
    				<div class="menu-button"><a class="route" data-id="ux" href="/ux" alt="ux">ux.</a></div>
                    <div class="menu-button"><a class="route" data-id="design" href="/design" alt="design">design.</a></div>
                    <div class="menu-button"><a class="route" data-id="code" href="/code" alt="code">code.</a></div>
                    <div class="menu-button"><a class="route" data-id="me" href="/me" alt="me">me.</a></div>
    			</div>
    		</div>
			<div id="main-ui-background"></div>

    	</div>

    	<!-- #template -->
    	<div id="template" class="empty">
    	</div>

    </div>
    <!-- #viewport -->

    <?php include 'templates/global/footer.php'; ?>
    
</body>
</html>
