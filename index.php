<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
    <?php
        if (isset($_SERVER['HTTP_USER_AGENT']) &&
        (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
            header('X-UA-Compatible: IE=edge,chrome=1');
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>projects</title>
</head>

<style>
	body, html {
		background: #222;
		height: 100%;
		max-width: 100%;
		overflow-x: hidden;
		color: #eee;
		font-family: Helvetica, Arial, Verdana, sans-serif;
		font-style: normal;
		font-size: 13px;
		line-height: 16px;
		-webkit-font-smoothing: antialiased;
		margin: 0;
		padding: 0;
	}
	p {
		padding: 1px;
		color: #6db3db;
	}
	.document {
		padding: 40px;
	}
	.section {
		padding: 8px 0;
	}
	h1.title {
		padding: 0 0 20px 0;
		margin: 0;
		font-size: 32px;
	}
	a, a:visited, a:hover, a:active {
		color: #ddd;
	}
	li {
		color: #ddd;
		padding: 5px 0 0 10px;
	}
	.breadcrumbs {
		padding: 0 0 30px 0;
	}
	a.crumb {
		color: #eee;
		font-size: 11px;
		padding: 0;
	}
	span {
		color: #666;
		padding: 0px 2px;
	}
</style>

<body>
    
    <!-- Document -->
    <div class="document">
		<h1 class="title">portfolio</h1>
		<div class="breadcrumbs">
			<a class="crumb" href="/">projects</a>
			<span>></span>
			<a class="crumb" href="/mike-stone">mike stone</a>
			<span>></span>
			<a class="crumb" href="/mike-stone/portfolio">portfolio</a>
		</div>
		<div class="section">
			<p>project versions:</p>
			<ul>
				<li><a class="" href="/mike-stone/portfolio/1.0">1.0</a></li>
			</ul>
		</div>
		<div class="section">
			<p>version notes:</p>
			<ul>
				<li>1.0 - Initial project set up.</li>
			</ul>
		</div>
    </div>

</body>
</html>
